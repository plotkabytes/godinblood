<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model frontend\models\StudyResult */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="row">
    <div class="col-lg-6 col-md-8 col-xs-12 center-block">

        <div class="study-result-form">


            <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'erythrocytes')->textInput() ?>

            <?= $form->field($model, 'hematocrit')->textInput() ?>

            <?= $form->field($model, 'hemoglobin')->textInput() ?>

            <?= $form->field($model, 'leukocytes')->textInput() ?>

            <?= $form->field($model, 'lymphocytes')->textInput() ?>

            <?= $form->field($model, 'pressure')->textInput() ?>

            <?= $form->field($model, 'systolic_pressure')->textInput() ?>

            <?= $form->field($model, 'diastolic_pressure')->textInput() ?>

            <?= $form->field($model, 'pulse')->textInput() ?>

            <?= $form->field($model, 'study_date')->widget(\yii\jui\DatePicker::classname(), [
              'language' => 'pl',
              'dateFormat' => 'yyyy-MM-dd',
              'options' => ['class' => 'form-control']
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Dodaj') : Yii::t('app', 'Zmień'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    </div>
</div>