<?php

/* @var $this yii\web\View */
$this->title = Yii::t('app', Yii::$app->name);
?>
<div class="site-index container-fluid">
  <div class="row">
    <div class="col-lg-5 col-md-5 col-xs-6 home-container">
      <div class="row">
        <h2>Mapa aktualnych punktów poboru krwii</h2>
        <!--- It should be responsive --->
        <?php echo $map; ?>
      </div>
      <div class="row">
        <h2>Aktualne ilosci krwii w centrach krwiodastwa</h2>
        <?php echo $amounts; ?>
        <!--- tu bedzie knob  https://almsaeedstudio.com/AdminLTE -> charts --->
      </div>
    </div>
    <div class="col-lg-5 col-lg-offset-2 col-md-5 col-md-offset-2 col-xs-6 home-container">
    </div>
  </div>
</div>
