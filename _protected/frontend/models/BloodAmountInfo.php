<?php

namespace frontend\models;

use Yii;
use yii\web\Link;
use yii\web\Linkable;
use yii\helpers\Url;

/**
 * This is the model class for table "blood_amount_info".
 *
 * @property integer $id
 * @property integer $place_id
 * @property integer $group_id
 * @property integer $amount_type_id
 * @property integer $parsedat
 *
 * @property Place $place
 * @property ParsedAmountType $amountType
 * @property BloodGroup $group
 */
class BloodAmountInfo extends \yii\db\ActiveRecord implements Linkable
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blood_amount_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['place_id', 'group_id', 'amount_type_id', 'parsedat'], 'required'],
            [['place_id', 'group_id', 'amount_type_id', 'parsedat'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'place_id' => Yii::t('app', 'Place ID'),
            'group_id' => Yii::t('app', 'Group ID'),
            'amount_type_id' => Yii::t('app', 'Amount Type ID'),
            'parsedat' => Yii::t('app', 'Parsedat'),
        ];
    }

    /**
     *  Filter fields for the REST API
     *  @return $fields
     */
    public function fields()
    {
        $fields = parent::fields();

        unset($fields['id'],
              $fields['parsedat']
            );

        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlace()
    {
        return $this->hasOne(Place::className(), ['id' => 'place_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmountType()
    {
        return $this->hasOne(ParsedAmountType::className(), ['id' => 'amount_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(BloodGroup::className(), ['id' => 'group_id']);
    }

    /**
     * @return REST link
     */
    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['bloodamountinfo/view', 'id' => $this->id], true),
            'place' => Url::to(['place/view','id' => $this->place_id], true),
            'amount_type' => Url::to(['parsedamounttype/view','id' => $this->amount_type_id], true),
            'group' => Url::to(['bloodgroup/view','id' => $this->group_id], true),
        ];
    }
}
