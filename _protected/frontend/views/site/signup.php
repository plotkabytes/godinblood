<?php
use nenad\passwordStrength\PasswordInput;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->title = Yii::t('app', 'Signup');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12 col-md-5 col-lg-5  login-container">

        <div class="login-body">

            <div class="login-title">
                <?= Html::img('/images/logo.png', ['class' => 'logo-img']); ?>
            </div>

            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <?= $form->field($model, 'username')->textInput(['placeholder' => Yii::t('app', 'Username')])->label(false) ?>
            <?= $form->field($model, 'email')->textInput(['placeholder' => Yii::t('app', 'Email')])->label(false) ?>
            <?= $form->field($model, 'password')->passwordInput(['placeholder' => Yii::t('app', 'Password')])->label(false) ?>

            <div class="form-group text-center row">

                <br>

                <div class="col-lg-6">
                    <?= Html::submitButton(Yii::t('app', 'Signup'), ['class' => 'btn login-button', 'name' => 'signup-button']) ?>
                </div>

                <div class="col-lg-6">
                    <?= Html::a(Yii::t('app', 'Back'), ['/site/login'], ['class' => 'btn login-button', 'name' => 'back-button']) ?>
                </div>

                <br>

            </div>

            <?php ActiveForm::end(); ?>

            <?php if ($model->scenario === 'rna'): ?>
                <div style="color:#666;margin:1em 0">
                    <i>*<?= Yii::t('app', 'We will send you an email with account activation link.') ?></i>
                </div>
            <?php endif ?>

        </div>
    </div>
</div>
