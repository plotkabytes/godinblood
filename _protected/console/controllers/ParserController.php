<?php

namespace console\controllers;

use yii\console\Controller;
use keltstr\simplehtmldom\SimpleHTMLDom as SHD;
use Yii;

/**
 * Parser controller
 * Usage from console: php yii parser/parse
 */
class ParserController extends Controller
{
    public $site = 'https://krwiodawcy.org/zapasy/';

    public $tag = 'img';

    // mapping to ID

    public $amount = [
        'krew0.png' => 1,
        'krew1.png' => 2,
        'krew2.png' => 3,
        'krew3.png' => 4,
        'krew4.png' => 5,
    ];

    public $places = [
        'Białystok'     => 1,
        'Bydgoszcz'     => 2,
        'Gdańsk'        => 3,
        'Katowice'      => 4,
        'Kalisz'        => 5,
        'Kielce'        => 6,
        'Kraków'        => 7,
        'Lublin'        => 8,
        'Łódź'          => 9,
        'Olsztyn'       => 10,
        'Opole'         => 11,
        'Poznań'        => 12,
        'Racibórz'      => 13,
        'Radom'         => 14,
        'Rzeszów'       => 15,
        'Słupsk'        => 16,
        'Wałbrzych'     => 17,
        'Warszawa'      => 18,
        'Zielona Góra'  => 19,
        'Wrocław'       => 20
    ];

    public $groups = [
        '0 Rh-'  => 1,
        '0 Rh+'  => 2,
        'A Rh-'  => 3,
        'A Rh+'  => 4,
        'B Rh-'  => 5,
        'B Rh+'  => 6,
        'AB Rh-' => 7,
        'AB Rh+' => 8
    ];


    /**
     * @param null $site strona ktora chcemy sparsowac
     * @param null $tag tag po ktorym wyszukujemy dane na stronie
     * @param bool $toFile czy chcemy zapisac do pliku
     */
    public function actionParse($site = null, $tag = null, $toFile = true)
    {
        $date = date_create();
        $timestamp = $date->getTimestamp();

        if($site != null) {
            $this->site = $site;
        }

        if($tag != null) {
            $this->tag = $tag;
        }

        $html = SHD::file_get_html($this->site);

        if (empty($html)) {

            echo PHP_EOL . 'Error while initialization... No html data...' . PHP_EOL . PHP_EOL;

        }
        else {

            echo PHP_EOL . 'Starting parsing...' . PHP_EOL . PHP_EOL;

            $index = 0;

            foreach($this->places as $place_key => $place_value) {
                echo 'Currently parsing: ' . $place_key . PHP_EOL;
                foreach($html->find($this->tag) as $img) {
                    if($img->alt === $place_key) {

                        $keys = array_keys($this->groups);

                        $group = $this->groups[$keys[$index]];
                        $amount = $this->amount[$img->src];

                        if($group == null || $amount == null) {
                            echo PHP_EOL . 'Error while searching for group or amount key...' . PHP_EOL . PHP_EOL;
                        }

                        $this->insertData($group,$amount,$place_value,$timestamp, $toFile);

                        $index++;
                    }

                }
                $index = 0;
            }

            echo PHP_EOL .'Parsing done.' . PHP_EOL . PHP_EOL;
        }

    }

    /**
     * @param $blood_group - grupa krwi
     * @param $status - status krwi
     * @param $place - miejsce gdzie oddano krew
     * @param $timestamp - data sparsowania danych
     * @param $toFile - czy zapisywac do pliku
     * @throws \yii\db\Exception standardowy error w przypadku dostepu do bazy
     */
    private function insertData($blood_group, $status, $place, $timestamp, $toFile)
    {
        $connection = Yii::$app->db;

        $connection->createCommand()->insert(
            '{{%blood_amount_info}}',
            [
                'place_id' => $place,
                'group_id' => $blood_group,
                'amount_type_id' => $status,
                'parsedat' => $timestamp,
            ]
        )->execute();

        if($toFile != false) {

            $myfile = fopen('logs/'.$timestamp.".txt", "a") or die("Unable to create file!");
            fwrite($myfile, 'Blood group: ' . $blood_group . ' Status: ' . $status . ' Place: ' . $place . PHP_EOL);
            fclose($myfile);

        }

    }

}
