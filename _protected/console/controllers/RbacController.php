<?php
namespace console\controllers;

use yii\helpers\Console;
use yii\console\Controller;
use Yii;

/**
 * Creates base rbac authorization data for our application.
 * -----------------------------------------------------------------------------
 */
class RbacController extends Controller
{
    /**
     * Initializes the RBAC authorization data.
     */
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        //---------- RULES ----------//

        // add the rule, author can edit only own data
        $rule = new \common\rbac\rules\AuthorRule;
        $auth->add($rule);

        //---------- ADMIN PERMISSIONS ----------//

        // add "manageUsers" permission
        $manageUsers = $auth->createPermission('manageUsers');
        $manageUsers->description = 'Allows admin+ roles to manage users';
        $auth->add($manageUsers);

        // add "manageDeclarations" permission
        $manageDeclarations = $auth->createPermission('manageDeclarations');
        $manageDeclarations->description = 'Allows admin+ roles to manage declarations';
        $auth->add($manageDeclarations);

        // add "manageBloodGroups" permission
        $manageBloodGroups = $auth->createPermission('manageBloodGroups');
        $manageBloodGroups->description = 'Allows admin+ roles to manage blood groups';
        $auth->add($manageBloodGroups);

        // add "manageBloodHistories" permission
        $manageBloodHistories = $auth->createPermission('manageBloodHistories');
        $manageBloodHistories->description = 'Allows admin+ roles to manage blood histories';
        $auth->add($manageBloodHistories);

        // add "manageBloodRequests" permission
        $manageBloodRequests = $auth->createPermission('manageBloodRequests');
        $manageBloodRequests->description = 'Allows admin+ roles to manage blood requests';
        $auth->add($manageBloodRequests);

        // add "manageBloodRequestsAnswers" permission
        $manageBloodRequestsAnswers = $auth->createPermission('manageBloodRequestsAnswers');
        $manageBloodRequestsAnswers->description = 'Allows admin+ roles to manage blood requests answers';
        $auth->add($manageBloodRequestsAnswers);

        // add "manageDonationTypes" permission
        $manageDonationTypes = $auth->createPermission('manageDonationTypes');
        $manageDonationTypes->description = 'Allows admin+ roles to manage blood donations types';
        $auth->add($manageDonationTypes);

        // add "manageParsedAmountTypes" permission
        $manageParsedAmountTypes = $auth->createPermission('manageParsedAmountTypes');
        $manageParsedAmountTypes->description = 'Allows admin+ roles to manage parsed amount types';
        $auth->add($manageParsedAmountTypes);

        // add "managePersonalInfos" permission
        $managePersonalInfos = $auth->createPermission('managePersonalInfos');
        $managePersonalInfos->description = 'Allows admin+ roles to manage user personal info';
        $auth->add($managePersonalInfos);

        // add "managePlaces" permission
        $managePlaces = $auth->createPermission('managePlaces');
        $managePlaces->description = 'Allows admin+ roles to manage places';
        $auth->add($managePlaces);

        // add "manageStudyResults" permission
        $manageStudyResults = $auth->createPermission('manageStudyResults');
        $manageStudyResults->description = 'Allows admin+ roles to manage user study result';
        $auth->add($manageStudyResults);

        //---------- USER PERMISSIONS ----------//

        // add the "manageOwn" permission and associate the rule with it.
        $manageOwn = $auth->createPermission('manageOwn');
        $manageOwn->description = 'Allows to manage only that content that belongs to the current user';
        $manageOwn->ruleName = $rule->name;
        $auth->add($manageOwn);

        // add the "updateOwnPersonalInfo" permission and associate the rule with it.
        $updateOwnPersonalInfo = $auth->createPermission('updateOwnPersonalInfo');
        $updateOwnPersonalInfo->description = 'Allows to update personal info that belongs to the current user';
        $updateOwnPersonalInfo->ruleName = $rule->name;
        $auth->add($updateOwnPersonalInfo);

        // add the "manageOwnStudyResult permission and associate the rule with it.
        $manageOwnStudyResult = $auth->createPermission('manageOwnStudyResult');
        $manageOwnStudyResult->description = 'Allows to manage studies results that belongs to the current user';
        $manageOwnStudyResult->ruleName = $rule->name;
        $auth->add($manageOwnStudyResult);

        // add the "manageOwnBloodRequest permission and associate the rule with it.
        $manageOwnBloodRequest = $auth->createPermission('manageOwnBloodRequest');
        $manageOwnBloodRequest->description = 'Allows to manage blood requests that belongs to the current user';
        $manageOwnBloodRequest->ruleName = $rule->name;
        $auth->add($manageOwnBloodRequest);

        // add the "manageOwnBloodRequestAnswer permission and associate the rule with it.
        $manageOwnBloodRequestAnswer = $auth->createPermission('manageOwnBloodRequestAnswer');
        $manageOwnBloodRequestAnswer->description = 'Allows to manage blood requests answers that belongs to the current user';
        $manageOwnBloodRequestAnswer->ruleName = $rule->name;
        $auth->add($manageOwnBloodRequestAnswer);

        // add the "manageOwnBloodHistory permission and associate the rule with it.
        $manageOwnBloodHistory = $auth->createPermission('manageOwnBloodHistory');
        $manageOwnBloodHistory->description = 'Allows to manage blood Histories that belongs to the current user';
        $manageOwnBloodHistory->ruleName = $rule->name;
        $auth->add($manageOwnBloodHistory);

        // add the "manageOwnBloodHistory permission and associate the rule with it.
        $manageOwnBloodDeclaration = $auth->createPermission('manageOwnBloodDeclaration');
        $manageOwnBloodDeclaration->description = 'Allows to manage blood Declarations that belongs to the current user';
        $manageOwnBloodDeclaration->ruleName = $rule->name;
        $auth->add($manageOwnBloodDeclaration);

        //---------- ROLES ----------//

        // add "member" role and give him privilages
        $member = $auth->createRole('member');
        $member->description = 'Registered users, members of this site';
        $auth->add($member);
        $auth->addChild($member,$manageOwn);
        $auth->addChild($member,$updateOwnPersonalInfo);
        $auth->addChild($member,$manageOwnStudyResult);
        $auth->addChild($member,$manageOwnBloodRequest);
        $auth->addChild($member,$manageOwnBloodRequestAnswer);
        $auth->addChild($member,$manageOwnBloodHistory);
        $auth->addChild($member,$manageOwnBloodDeclaration);

        // add "admin" role and give him privilages
        $admin = $auth->createRole('admin');
        $admin->description = 'Administrator';
        $auth->add($admin);
        $auth->addChild($admin, $manageUsers);
        $auth->addChild($admin, $manageDeclarations);
        $auth->addChild($admin, $manageBloodGroups);
        $auth->addChild($admin, $manageBloodHistories);
        $auth->addChild($admin, $manageBloodRequests);
        $auth->addChild($admin, $manageBloodRequestsAnswers);
        $auth->addChild($admin, $manageDonationTypes);
        $auth->addChild($admin, $manageParsedAmountTypes);
        $auth->addChild($admin, $managePersonalInfos);
        $auth->addChild($admin, $managePlaces);
        $auth->addChild($admin, $manageStudyResults);

        $theCreator = $auth->createRole('theCreator');
        $theCreator->description = 'Mateusz Zyla';
        $auth->add($theCreator);
        $auth->addChild($theCreator, $admin);

        if ($auth)
        {
            $this->stdout("\nRbac authorization data are installed successfully.\n", Console::FG_GREEN);
        }
    }
}
