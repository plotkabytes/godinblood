<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Blood Amount Info');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blood-amount-info-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Blood Amount Info'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'place_id',
            'group_id',
            'amount_type_id',
            'parsedat',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
