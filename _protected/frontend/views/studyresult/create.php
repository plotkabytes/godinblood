<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\StudyResult */

$this->title = Yii::t('app', 'Dodaj Study Result');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Study Results'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="study-result-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
