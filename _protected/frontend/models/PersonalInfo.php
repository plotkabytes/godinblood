<?php

namespace frontend\models;

use Yii;
use yii\web\Link;
use yii\web\Linkable;
use yii\helpers\Url;

/**
 * This is the model class for table "personal_info".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $gender_id
 * @property integer $weight
 *
 * @property User $user
 * @property Gender $gender
 */
class PersonalInfo extends \yii\db\ActiveRecord implements Linkable
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'personal_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'gender_id', 'weight','height'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'gender_id' => Yii::t('app', 'Gender ID'),
            'weight' => Yii::t('app', 'Weight'),
            'height' => Yii::t('app', 'Height'),
        ];
    }

    /**
     *  Filter fields for the REST API
     *  @return $fields
     */
    public function fields()
    {
        $fields = parent::fields();

        unset($fields['id']);

        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGender()
    {
        return $this->hasOne(Gender::className(), ['id' => 'gender_id']);
    }

    /**
     * @return REST link
     */
    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['personalinfo/view', 'id' => $this->id], true),
            'gender' => Url::to(['gender/view','id' => $this->gender_id], true),
            'user' => Url::to(['user/view','id' => $this->user_id], true),
        ];
    }
}
