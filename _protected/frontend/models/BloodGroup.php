<?php

namespace frontend\models;

use Yii;
use yii\web\Link;
use yii\web\Linkable;
use yii\helpers\Url;

/**
 * This is the model class for table "blood_group".
 *
 * @property integer $id
 * @property string $group
 *
 * @property BloodAmountInfo[] $bloodAmountInfos
 */
class BloodGroup extends \yii\db\ActiveRecord implements Linkable
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blood_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group'], 'required'],
            [['group'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'group' => Yii::t('app', 'Group'),
        ];
    }

    /**
     *  Filter fields for the REST API
     *  @return $fields
     */
    public function fields()
    {
        $fields = parent::fields();

        unset($fields['id']);

        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBloodAmountInfos()
    {
        return $this->hasMany(BloodAmountInfo::className(), ['group_id' => 'id']);
    }

    /**
     * @return REST link
     */
    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['bloodgroup/view', 'id' => $this->id], true),
        ];
    }

}
