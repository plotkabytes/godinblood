<?php
namespace frontend\widgets;

use frontend\models\DonationType;
use yii;
use yii\base\Widget;
use yii\helpers\Html;
use frontend\models\Place;
/**
 * Accordion widget.
 */
class Accordion extends Widget
{

    public $mainTitle;
    public $items;

    public function init()
    {
        parent::init();

    }

    public function run()
    {
        echo '<div class="box box-solid">';
        echo '<div class="box-header with-border">';
        echo '<h3 class="box-title">'.$this->mainTitle.'</h3>';
        echo '<div class="box-body">';
        echo '<div class="box-group" id="accordion">';

        $iterator = 0;

        foreach($this->items as $item)
        {
            echo '<div class="panel box box-primary"><div class="box-header with-border"><h4 class="box-title">';
            echo '<a data-toggle="collapse" data-parent="#accordion" href="#collapse'.$iterator.'" aria-expanded="true" class="">';
            echo Place::find()->where(['id' => $item->place_id])->one()->place;
            echo '</a></h4></div>';
            echo '<div id="collapse'.$iterator.'" class="panel-collapse collapse in" aria-expanded="true">';
            echo '<div class="box-body">';
            echo empty($item->reason) ? Yii::t('app','Oddalem ').$item->amount.Yii::t('app','ml krwi typu ').DonationType::find()->where(['id' => $item->type_id])->one()->type :
                Yii::t('app','Nie moglem oddac krwi z powodu: ').$item->reason;
            echo '</div>';
            echo '</div>';
            echo '</div>';

            $iterator++;
        }

        echo '</div>';
        echo '</div>';
        echo Html::a(Yii::t('app', 'Create Blood History'), ['create'], ['class' => 'btn btn-success pull-right']);
        echo '</div>';
    }
}


