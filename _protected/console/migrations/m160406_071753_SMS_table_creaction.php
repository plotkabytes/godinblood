<?php

use yii\db\Migration;

class m160406_071753_SMS_table_creaction extends Migration
{
    public function up()
    {

      $tableOptions = null;

      if ($this->db->driverName === 'mysql')
      {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%SMS_info}}', [
          'id' => $this->primaryKey(),
          'user_id' => $this->integer()->notNull(),
          'was_sent' => $this->boolean(),
          'sent_date' => $this->date(),
          'FOREIGN KEY (user_id) REFERENCES {{%user}}(id)
              ON DELETE CASCADE ON UPDATE CASCADE',
      ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%SMS_info}}');
    }

}
