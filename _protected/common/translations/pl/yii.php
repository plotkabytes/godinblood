<?php

return [
    '{attribute} cannot be blank.' => '{attribute} nie może być puste.',
    'The verification code is incorrect.' => 'Niepoprawny kod weryfikacyjny.',
    'Home' => 'Głowna',
    'You are not allowed to perform this action.' => 'Nie posiadasz wystarczających uprawnień do wykonania tej akcji.'
];
