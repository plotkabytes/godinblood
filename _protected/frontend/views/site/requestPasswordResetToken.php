<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

$this->title = Yii::t('app', 'Request password reset');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12 col-md-5 col-lg-5  login-container">
        <div class="login-body">
            <div class="login-title">
                <?= Html::img('/images/logo.png', ['class' => 'logo-img']); ?>
            </div>

            <p><?= Yii::t('app', 'A link to reset password will be sent to your email.') ?></p>

            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

            <?= $form->field($model, 'email')->textInput(['placeholder' => Yii::t('app', 'Please fill out your email.')])->label(false) ?>

            <div class="form-group text-center row">

                <br>

                <div class="col-lg-6">
                    <?= Html::submitButton(Yii::t('app', 'Reset Password'), ['class' => 'btn login-button', 'name' => 'signup-button']) ?>
                </div>

                <div class="col-lg-6">
                    <?= Html::a(Yii::t('app', 'Back'), ['/site/login'], ['class' => 'btn login-button', 'name' => 'back-button']) ?>
                </div>

                <br>

            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

