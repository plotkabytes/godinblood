<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\BloodDeclaration */

$this->title = Yii::t('app', 'Dodaj Blood Declaration');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blood Declarations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blood-declaration-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
