<?php

namespace api\modules\v1\controllers;

use yii\filters\auth\HttpBearerAuth;
use yii\web\ForbiddenHttpException;
use yii\rest\ActiveController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\models\User;
use frontend\models\BloodGroup;
use Yii;

/**
 * BloodgroupController is class used to
 * get info about blood groups
 * @author Mateusz Żyła
 */
class BloodgroupController extends ActiveController
{
    public $modelClass = 'frontend\models\BloodGroup';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }

    /**
     * Get all data from blood group table and send it to the user
     *
     * ```
     * HEADER: Authorization : Bearer $token
     * GET http://bloodgang.local/api/v1/bloodgroup
     * ```
     *
     * @return JSON with $info
     */
    public function actionIndex()
    {
        $info = BloodGroup::find()->asArray()->all();
        return $info;
    }
}
