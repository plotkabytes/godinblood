<?php
use frontend\assets\UserpanelAssets;
use frontend\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
/* @var $this \yii\web\View */
/* @var $content string */

UserpanelAssets::register($this);

$this->title = Yii::t('app', 'Dashboard');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="hold-transition <?= Yii::$app->session['sidebar'] == 1 ? 'sidebar-collapse' : '' ?> sidebar-mini skin-red">
  <?php $this->beginBody() ?>
  <div class="wrapper">
    <header class="main-header">

      <!-- this should be in widget -->
      <div class="logo">

          <span class="logo-mini"><?= Html::img('/images/small-logo.png', ['class' => 'small-logo-img']); ?></span>
        <span class="logo-lg"><?= Html::img('/images/logo.png', ['class' => 'logo-img']); ?></span>
      </div>

      <!-- this should be in widget -->
      <nav class="navbar navbar-static-top" role="navigation">
        <?= Html::a('', '#', [
            'class' => 'sidebar-toggle',
            'data-toggle' => 'offcanvas',
            'role' => 'button'
        ]) ?>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
      		  <li class="dropdown user user-menu">
      			<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
      			  <span class="hidden-xs"><?= Yii::t('app','Logged as '). Yii::$app->user->identity->username ?></span>
      			</a>
      			<ul class="dropdown-menu">
      			  <li class="user-footer">
      				<div class="pull-right">
                <?= Html::a(Yii::t('app','Profile'), '/site/profile', ['class' => 'btn btn-default btn-flat btn-dropdown-menu']) ?>
                <?= Html::a(Yii::t('app','Sign out'), '/site/logout', ['class' => 'btn btn-default btn-flat btn-dropdown-menu',
                            'data-method' => 'post']) ?>
      				</div>
      			  </li>
      			</ul>
      		  </li>
          </ul>
        </div>
      </nav>
    </header>

    <aside class="main-sidebar">
      <section class="sidebar">

        <?= Html::beginForm('#', 'get', ['class' => 'sidebar-form']) ?>
          <div class="input-group">
            <?= Html::input('text','searchengine',Yii::t('app','Search...'), ['class' => 'form-control']) ?>
            <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
            </span>
          </div>
        <?= Html::endForm() ?>

        <?= frontend\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu'],
                        'items' => [
                            ['label' => Yii::t('app','Menu'), 'options' => ['class' => 'header']],
                            ['label' => Yii::t('app','Statistic'), 'icon' => 'fa fa-bar-chart', 'url' => ['site/panel']],
                            ['label' => Yii::t('app','BloodHistory'), 'icon' => 'fa fa-pencil-square-o', 'url' => ['bloodhistory/index']],
                            ['label' => Yii::t('app','Studies'), 'icon' => 'fa fa-tasks','url' => ['studyresult/index']],
                            [
                                'label' => Yii::t('app','Requests'),
                                'icon' => 'fa fa-exclamation',
                                'url' => '#',
                                'items' => [
                                    ['label' => Yii::t('app','Overall Requests'), 'icon' => 'fa fa-book', 'url' => ['bloodrequest/overall'],],
                                    ['label' => Yii::t('app','My Requests'), 'icon' => 'fa fa-share', 'url' => ['bloodrequest/requests'],],
                                    ['label' => Yii::t('app','My Answers'), 'icon' => 'fa fa-reply', 'url' => ['bloodrequestanswers/answers'],],
                                ],
                            ],
                        ],
                    ]
                ) ?>
      </section>
    </aside>

    <div class="content-wrapper">
      <section class="content-header">

        <?= Breadcrumbs::widget([
            'homeLink' => [
                    'label' => Yii::t('yii', 'Dashboard'),
                    'url' => Yii::$app->homeUrl,
                    'template' => "<li><i class='fa fa-dashboard'></i></li>\n"
                ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

      </section>

      <section class="content">
          <?= $content ?>
      </section>
    </div>

      <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <?= Yii::powered() ?>
      </div>
      &copy; <?= Yii::t('app', Yii::$app->name) ?> <?= date('Y') ?>
    </footer>
    <div class="control-sidebar-bg" style="position: fixed; height: auto;"></div>
  </div>
  <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
