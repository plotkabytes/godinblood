<?php
namespace frontend\controllers;

use common\models\User;
use common\models\LoginForm;

use frontend\models\BloodAmountInfo;
use frontend\models\BloodDeclaration;
use frontend\models\BloodHistory;
use frontend\models\Panel;

use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;

use frontend\models\StudyResult;
use yii\helpers\Html;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;

use Yii;

/**
 * Site controller.
 * Odpowiada za rejestracje i logowanie
 */
class SiteController extends AccessController
{

    public $layout = "main";

    /**
     * Deklaruje zewnetrzne akcje
     *
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

//------------------------------------------------------------------------------------------------//
// STATIC PAGES
//------------------------------------------------------------------------------------------------//

    /**
     *  Wyswietla glowna strone
     *
     * @return string
     */
    public function actionIndex()
    {
        // for current amounts of blood
        //$latest_blood_info  = BloodAmountInfo::find()->max('parsedat')->all();

        // for displaying latest blood study result
        //$latest_blood_study = StudyResult::find()->max('study_date')->one();

        // for displaying latest blood declaration
        //$lastest_blood_declaration = BloodDeclaration::find()->max('id')->one();

        // map with locations
        $map = Panel::getMap(400, 400 , 52.399, 16.900, 10);

        if(Yii::$app->user->getIsGuest()) {
            return $this->redirect('/site/login');
        }
        else {
            return $this->redirect('/site/panel');
        }

    }

    /**
     * non implemented yet
     * @return \yii\web\Response
     */
    public function actionProfile()
    {
        if(Yii::$app->user->getIsGuest()) {
            return $this->redirect('/site/login');
        }
        else {
            return $this->redirect('/site/panel');
        }

    }

//------------------------------------------------------------------------------------------------//
// LOG IN / LOG OUT / PASSWORD RESET
//------------------------------------------------------------------------------------------------//

    /**
     * Loguje uzytkownika
     *
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        $this->layout = 'main';

        if (!Yii::$app->user->isGuest)
        {
            return $this->redirect('/site/panel');
        }

        // get setting value for 'Login With Email'
        $lwe = Yii::$app->params['lwe'];

        // if 'lwe' value is 'true' we instantiate LoginForm in 'lwe' scenario
        $model = $lwe ? new LoginForm(['scenario' => 'lwe']) : new LoginForm();

        // now we can try to log in the user
        if ($model->load(Yii::$app->request->post()) && $model->login())
        {
            Yii::$app->session->set('userSessionTimeout', time() + Yii::$app->params['sessionTimeoutSeconds']);

            return $this->redirect('/site/panel');
        }
        // user couldn't be logged in, because he has not activated his account
        elseif($model->notActivated())
        {
            // if his account is not activated, he will have to activate it first
            Yii::$app->session->setFlash('error',
                Yii::t('app','Cannot login before account activation, please check email'));

            return $this->refresh();
        }
        // account is activated, but some other errors have happened
        else
        {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the user.
     *
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        $this->layout = 'main';

        Yii::$app->user->logout();

        return $this->goHome();
    }

/*----------------*
 * PASSWORD RESET *
 *----------------*/

    /**
     * Wysyla Email z resetem hasla
     *
     * @return string|\yii\web\Response
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            if ($model->sendEmail())
            {
                Yii::$app->session->setFlash('success',
                    Yii::t('app','Please check your email for further informations.'));

                return $this->goHome();
            }
            else
            {
                Yii::$app->session->setFlash('error',
                    Yii::t('app','We have problem here... Sorry'));
            }
        }
        else
        {
            return $this->render('requestPasswordResetToken', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Resetowanie hasla
     *
     * @param  string $token Password reset token.
     * @return string|\yii\web\Response
     *
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = 'main';

        try
        {
            $model = new ResetPasswordForm($token);
        }
        catch (InvalidParamException $e)
        {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post())
            && $model->validate() && $model->resetPassword())
        {
            Yii::$app->session->setFlash('success', Yii::t('app','Password saved.'));

            return $this->redirect('/site/panel');
        }
        else
        {
            return $this->render('resetPassword', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Rejestracja uzytkownika.
     *
     * @return string|\yii\web\Response
     */
    public function actionSignup()
    {
        $this->layout = 'main';

        // get setting value for 'Registration Needs Activation'
        $rna = Yii::$app->params['rna'];

        // if 'rna' value is 'true', we instantiate SignupForm in 'rna' scenario
        $model = $rna ? new SignupForm(['scenario' => 'rna']) : new SignupForm();

        // collect and validate user data
        if ($model->load(Yii::$app->request->post()) && $model->validate())
        {
            // try to save user data in database
            if ($user = $model->signup())
            {
                // if user is active he will be logged in automatically ( this will be first user )
                if ($user->status === User::STATUS_ACTIVE)
                {
                    if (Yii::$app->getUser()->login($user))
                    {
                        User::createPersonalInfo($user);

                        return $this->redirect('login');
                    }
                }
                // activation is needed, use signupWithActivation()
                else
                {
                    $this->signupWithActivation($model, $user);

                    User::createPersonalInfo($user);

                    return $this->refresh();
                }
            }
            // user could not be saved in database
            else
            {
                // display error message to user
                Yii::$app->session->setFlash('error',
                    Yii::t('app','Ups! We have problem here! Please contact us to activate your account'));

                // log this error, so we can debug possible problem easier.
                Yii::error(Yii::t('app','Ups! We have problem here! Please contact us to register your account'));
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Rejestracja z aktywacja
     * Uzytkownik musi kliknac w link wyslany przez email
     *
     * @param $model
     * @param $user
     */
    private function signupWithActivation($model, $user)
    {
        // try to send account activation email
        if ($model->sendAccountActivationEmail($user))
        {
            Yii::$app->session->setFlash('success',
                'Hello '.Html::encode($user->username).'.
                 Before you can login, you should activate your account by email.');
        }
        // email could not be sent
        else
        {
            // display error message to user
            Yii::$app->session->setFlash('error',
                "We could not send you a message with activation link, please contact us!.");

            // log this error, so we can debug possible problem easier.
            Yii::error('Ups, we got problem here!
                User '.Html::encode($user->username).' could not be registered.
                Please contact us!.');
        }
    }

    /**
     * Aktywacja uzytkownika
     *
     * @param  string $token
     * @return \yii\web\Response
     *
     * @throws BadRequestHttpException
     */
    public function actionActivateAccount($token)
    {
        try
        {
            $user = new AccountActivation($token);
        }
        catch (InvalidParamException $e)
        {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($user->activateAccount())
        {
            Yii::$app->session->setFlash('success',
                'Yeah! You can login now.
                 Thanks '.Html::encode($user->username).' for registration!');
        }
        else
        {
            Yii::$app->session->setFlash('error',
                ''.Html::encode($user->username).' We couldnt register you, please try again latter!');
        }

        return $this->redirect('login');
    }

    /** Przekierowuje na panel
     * @return string
     */
    public function actionPanel()
     {
        Yii::$app->session['sidebar'] = 0;
        $this->layout = 'panel';

         // for current amounts of blood
         $latest_blood_info  = BloodAmountInfo::find()->max('parsedat');

         // for displaying latest blood study result
         //$latest_blood_study = StudyResult::find()->max('study_date')->one();

         // for displaying latest blood declaration
         //$lastest_blood_declaration = BloodDeclaration::find()->max('id')->one();

         // map with locations
         $map = Panel::getMap(500, 1450 , 52.399, 16.900, 8);
         
         $amounts = Panel::getAmounts();
         
         return $this->render('/site/panel', [
             //'latest_blood_info' => $latest_blood_info,
             //'latest_blood_study' => $latest_blood_study,
             //'lastest_blood_declaration' => $lastest_blood_declaration,
             'map' => $map,
             'amounts' => $amounts
         ]);
     }

}
