<?php

namespace api\modules\v1\controllers;

use yii\filters\auth\HttpBearerAuth;
use yii\web\ForbiddenHttpException;
use yii\rest\ActiveController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\models\User;
use frontend\models\DonationType;
use Yii;

/**
 * DonationtypeController is class used to
 * get donation types
 *
 * @author Mateusz Żyła
 */
class DonationtypeController extends ActiveController
{
    public $modelClass = 'frontend\models\DonationType';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }

    /**
     * Get all data from donation type table and send it to the user
     *
     * ```
     * HEADER: Authorization : Bearer $token
     * GET http://bloodgang.local/api/v1/donationtype
     * ```
     *
     * @return JSON with $info
     */
    public function actionIndex()
    {
        $info = DonationType::find()->asArray()->all();
        return $info;
    }

}
