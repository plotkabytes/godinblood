<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\DonationType */

$this->title = Yii::t('app', 'Zmień {modelClass}: ', [
    'modelClass' => 'Donation Type',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Donation Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Zmień');
?>
<div class="donation-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
