<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = Yii::t('app', 'Login');
?>

<div class="col-xs-12 col-md-5 col-lg-5  login-container">

    <div class="login-body">

        <div class="login-title">
            <?= Html::img('/images/logo.png', ['class' => 'logo-img']); ?>
        </div>

        <?php $form = ActiveForm::begin([
            'fieldConfig' => ['options' => ['class' => 'form-group has-feedback']],
        ]); ?>

        <?php //-- use email or username field depending on model scenario --// ?>
        <?php if ($model->scenario === 'lwe'): ?>
            <?= $form->field($model, 'email')->textInput(['placeholder' => Yii::t('app', 'Email')])->label(false); ?>
        <?php else: ?>
            <?= $form->field($model, 'username')->textInput(['placeholder' => Yii::t('app', 'Username')])->label(false); ?>
        <?php endif ?>

        <?= $form->field($model, 'password')->passwordInput(['placeholder' => Yii::t('app', 'Password')])->label(false); ?>

        <div class="form-group text-center">
            <br>
            <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn login-button', 'name' => 'login-button']) ?>
        </div>

        <div class="login-bottom">
            <div class="row text-center">
                <?= Yii::t('app', 'If you dont have account you can') ?>
                <strong><?= Html::a(Yii::t('app', 'create it'), ['site/signup']) ?>.</strong>
            </div>
            <div class="row text-center">
                <?= Yii::t('app', 'If you forgot your password you can') ?>
                <strong><?= Html::a(Yii::t('app', 'reset it'), ['site/request-password-reset']) ?>.</strong>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
</div>
