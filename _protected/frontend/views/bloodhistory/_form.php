<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\Place;
use frontend\models\DonationType;

/* @var $this yii\web\View */
/* @var $model frontend\models\BloodHistory */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="row">
    <div class="col-lg-6 col-md-8 col-xs-12 center-block">


<div class="blood-history-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'place_id')->dropDownList(ArrayHelper::map(Place::find()->all(), 'id', 'place')) ?>

    <?= $form->field($model, 'type_id')->dropDownList(ArrayHelper::map(DonationType::find()->all(), 'id', 'type')) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'reason')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Dodaj') : Yii::t('app', 'Zmień'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

        </div>
    </div>