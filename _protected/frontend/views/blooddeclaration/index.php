<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Blood Declaration');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box-header">
    <p>
        <?= Html::a(Yii::t('app', 'Create Blood Declaration'), ['create'], ['class' => 'btn btn-success btn-lg pull-left']) ?>
    </p>
</div>

<div class="row">

    <?php
    foreach($dataProvider as $row)
    {
        echo '<div class="col-md-3">';
        echo '   <div class="box box-warning box-solid">';
        echo '       <div class="box-header with-border">';
        echo '           <h3 class="box-title"></h3>';/*.$row['place'].*/
        echo '           <div class="box-tools pull-right">';
        echo '               <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>';
        echo '               </button>';
        echo '           </div>';
        echo '           <!-- /.box-tools -->';
        echo '       </div>';
        echo '       <!-- /.box-header -->';
        echo '       <div class="box-body">';
        echo $row['type'];
        echo '       </div>';
        echo '       <!-- /.box-body -->';
        echo '   </div>';
        echo '</div>';
    }
    ?>

</div>
