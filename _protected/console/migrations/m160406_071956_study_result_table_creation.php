<?php

use yii\db\Migration;

class m160406_071956_study_result_table_creation extends Migration
{
    public function up()
    {

      $tableOptions = null;

      if ($this->db->driverName === 'mysql')
      {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%study_result}}', [
          'id' => $this->primaryKey(),
          'user_id' => $this->integer()->notNull(),
          'erythrocytes' => $this->float(),
          'hematocrit' => $this->float(),
          'hemoglobin' => $this->float(),
          'leukocytes' => $this->float(),
          'lymphocytes' => $this->float(),
          'pressure' => $this->float(),
          'lymphocytes' => $this->float(),
          'systolic_pressure' => $this->float(),
          'diastolic_pressure' => $this->float(),
          'pulse' => $this->float(),
          'study_date' => $this->date()->notNull(),
          'FOREIGN KEY (user_id) REFERENCES {{%user}}(id)
              ON DELETE CASCADE ON UPDATE CASCADE',
      ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%study_result}}');
    }
}
