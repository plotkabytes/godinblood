<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\ParsedAmountType */

$this->title = Yii::t('app', 'Dodaj Parsed Amount Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Parsed Amount Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parsed-amount-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
