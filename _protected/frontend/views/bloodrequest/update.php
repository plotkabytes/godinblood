<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\BloodRequest */

$this->title = Yii::t('app', 'Zmień {modelClass}: ', [
    'modelClass' => 'Blood Request',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blood Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Zmień');
?>
<div class="blood-request-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
