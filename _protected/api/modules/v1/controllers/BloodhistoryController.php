<?php

namespace api\modules\v1\controllers;

use yii\web\ForbiddenHttpException;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\models\User;
use frontend\models\BloodHistory;
use Yii;

/**
 * BloodhistoryController is class used to
 * get blood history
 *
 * @author Mateusz Żyła
 */
class BloodhistoryController extends ActiveController
{
    public $modelClass = 'frontend\models\BloodHistory';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);

        return $actions;
    }

    /**
     * Get data specified for uset from blood history table and send it to the user
     *
     * ```
     * HEADER: Authorization : Bearer $token
     * GET http://bloodgang.local/api/v1/bloodhistory
     * ```
     *
     * @return JSON with $info
     */
    public function actionIndex()
    {
        return BloodHistory::find()->asArray()->where(['user_id' => Yii::$app->user->identity->id])->all();
    }

    /**
     * Checks the privilege of the current user.
     *
     * This method should be overridden to check whether the current user has the privilege
     * to run the specified action against the specified data model.
     * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
     *
     * @param string $action the ID of the action to be executed
     * @param \yii\base\Model $model the model to be accessed. If null, it means no specific model is being accessed.
     * @param array $params additional parameters
     * @throws ForbiddenHttpException if the user does not have access
     */
    public function checkAccess($action, $model = null, $params = [])
    {
        if($action == 'update' || $action == 'view' || $action == 'delete' || $action == 'index' & $model != null) {
            if($model->user_id == Yii::$app->user->identity->id) {
                return true;
            }
            else {
                throw new ForbiddenHttpException();
            }
        }
        return true;
    }

}
