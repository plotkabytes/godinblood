<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\models\BloodRequest;

/* @var $this yii\web\View */
/* @var $model frontend\models\BloodRequestAnswers */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="row">
    <div class="col-lg-6 col-md-8 col-xs-12 center-block">


<div class="blood-request-answers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'request_id')->dropDownList(ArrayHelper::map(BloodRequest::find()->all(), 'id', 'for_who')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Dodaj') : Yii::t('app', 'Zmień'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

        </div>
    </div>