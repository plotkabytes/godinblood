<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\StudyResult */

$this->title = Yii::t('app', 'Zmień {modelClass}: ', [
    'modelClass' => 'Study Result',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Study Results'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Zmień');
?>
<div class="study-result-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
