<?php

namespace frontend\models;

use Yii;
use yii\web\Link;
use yii\web\Linkable;
use yii\helpers\Url;

/**
 * This is the model class for table "study_result".
 *
 * @property integer $id
 * @property integer $user_id
 * @property double $erythrocytes
 * @property double $hematocrit
 * @property double $hemoglobin
 * @property double $leukocytes
 * @property double $lymphocytes
 * @property double $pressure
 * @property double $systolic_pressure
 * @property double $diastolic_pressure
 * @property double $pulse
 * @property string $study_date
 *
 * @property User $user
 */
class StudyResult extends \yii\db\ActiveRecord implements Linkable
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'study_result';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'study_date'], 'required'],
            [['user_id'], 'integer'],
            [['erythrocytes', 'hematocrit', 'hemoglobin', 'leukocytes', 'lymphocytes', 'pressure', 'systolic_pressure', 'diastolic_pressure', 'pulse'], 'number'],
            [['study_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'erythrocytes' => Yii::t('app', 'Erythrocytes'),
            'hematocrit' => Yii::t('app', 'Hematocrit'),
            'hemoglobin' => Yii::t('app', 'Hemoglobin'),
            'leukocytes' => Yii::t('app', 'Leukocytes'),
            'lymphocytes' => Yii::t('app', 'Lymphocytes'),
            'pressure' => Yii::t('app', 'Pressure'),
            'systolic_pressure' => Yii::t('app', 'Systolic Pressure'),
            'diastolic_pressure' => Yii::t('app', 'Diastolic Pressure'),
            'pulse' => Yii::t('app', 'Pulse'),
            'study_date' => Yii::t('app', 'Study Date'),
        ];
    }

    /**
     *  Filter fields for the REST API
     *  @return $fields
     */
    public function fields()
    {
        $fields = parent::fields();

        unset($fields['id']);

        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return REST link
     */
    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['studyresult/view', 'id' => $this->id], true),
            'user' => Url::to(['user/view', 'id' => $this->user_id], true),
        ];
    }
}
