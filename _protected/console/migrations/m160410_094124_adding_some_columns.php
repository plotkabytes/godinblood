<?php

use yii\db\Migration;

class m160410_094124_adding_some_columns extends Migration
{
    public function up()
    {
      $this->addColumn('{{%blood_history}}','date',$this->date());
      $this->addColumn('{{%blood_declaration}}','date',$this->date());

      /* this one is optional */
      $this->addColumn('{{%blood_request_answers}}','answer',$this->string());
    }

    public function down()
    {
      $this->removeColumn('{{%blood_history}}','date',$this->date());
      $this->removeColumn('{{%blood_declaration}}','date',$this->date());

      /* this one is optional */
      $this->removeColumn('{{%blood_request_answers}}','answer',$this->string());

      return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
