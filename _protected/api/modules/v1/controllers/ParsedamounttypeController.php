<?php

namespace api\modules\v1\controllers;

use yii\filters\auth\HttpBearerAuth;
use yii\web\ForbiddenHttpException;
use yii\rest\ActiveController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\models\User;
use frontend\models\ParsedAmountType;
use Yii;

/**
 * ParsedamounttypeController is class used to
 * get parsed amount types of blood
 *
 * @author Mateusz Żyła
 */
class ParsedamounttypeController extends ActiveController
{
    public $modelClass = 'frontend\models\ParsedAmountType';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];

        return $behaviors;
    }

    public function actions()
    {
      $actions = parent::actions();

      unset($actions['index']);
      unset($actions['view']);
      unset($actions['create']);
      unset($actions['update']);
      unset($actions['delete']);

      return $actions;
    }

    /**
     * Get all data from parsed amount type table and send it to the user
     *
     * ```
     * HEADER: Authorization : Bearer $token
     * GET http://bloodgang.local/api/v1/parsedamounttype
     * ```
     *
     * @return JSON with $info
     */
    public function actionIndex()
    {
        $info = ParsedAmountType::find()->asArray()->all();
        return $info;
    }

}
