<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\PersonalInfo */

$this->title = Yii::t('app', 'Zmień {modelClass}: ', [
    'modelClass' => 'Personal Info',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Personal Infos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Zmień');
?>
<div class="personal-info-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
