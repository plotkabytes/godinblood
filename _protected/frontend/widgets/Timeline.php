<?php
namespace frontend\widgets;

use yii;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class table
 * Theme table widget.
 */
class Timeline extends Widget
{

    public $fields;
    public $items;

    public $timeField;
    public $displayTime;

    public $headerField;
    public $displayHeader;

    public $iconField;
    public $displayIcon;

    public $displayFooter;
    public $noItemsMessage;
    public $dateField;

    public $colors;
    public $icons;

    public function init()
    {
        parent::init();

        if($this->noItemsMessage === null) {
            $this->noItemsMessage = 'You have no items to display';//Yii::t('tableWidget','You have no items to display');
        }

        if($this->displayFooter === null) {
            $this->displayFooter = true;
        }

        if($this->displayTime === null) {
            $this->displayTime = false;
        }

        if($this->displayIcon === null) {
            $this->displayIcon = true;
        }

        if($this->colors === null) {
            $this->colors = array(
                'bg-yellow',
                'bg-red',
                'bg-green',
                'bg-aqua'
            );
        }

        if($this->icons === null) {
            $this->icons = array(
                'fa-bar-chart',
                'fa-eyedropper',
                'fa-info-circle',
                'fa-bookmark-o'
            );
        }

    }

    public function run()
    {
        if(empty($this->items)) {
            return Html::decode('<div class="row text-center">
                                <div class="col-lg-6 col-md-8 col-xs-12 center-block">
                                <div class="callout callout-warning"><h4>'.Yii::t('app',$this->noItemsMessage).'</h4>
                                <p>'.Yii::t('app', 'Maybe you want to').'</p>'.
                                Html::a(Yii::t('app', 'create one?'), ['create'])
                                .'</div>
                                </div>
                                </div>');
        }

        echo '<ul class="timeline">';

        foreach( $this->items as $item ) {

            echo '<li class="time-label"><span class="bg-blue">';

            if(!empty($item[$this->dateField]))
                echo Html::encode($item[$this->dateField]);

            echo '</span></li><li>';

            if($this->displayIcon) {
                if(!empty($item[$this->iconField]))
                    echo '<i class="fa bg-blue">' . Html::encode($item[$this->iconField]) . '</i>';
                else
                    echo '<i class="fa bg-red fa-heart"></i>';
            }

            echo '<div class="timeline-item">';

            if($this->displayTime != false) {

                echo '<span class="time"><i class="fa fa-clock-o"></i>';

                if (!empty($item[$this->timeField]))
                    echo Html::encode($item[$this->timeField]);

                echo '</span>';
            }

            if($this->displayHeader != false) {

                echo '<h3 class="timeline-header"><a href="#">';

                if(!empty($item[$this->headerField]))
                    echo Html::encode($item[$this->headerField]);

                echo '</a></h3>';

            }
            echo '<div class="timeline-body">';
            echo '<div class="row timeline-row">';
            echo '<br>';

            $colors_iterator = 0;

            foreach ($this->fields as $field) {

                if($colors_iterator == sizeof($this->colors) - 1)
                    $colors_iterator = 0;
                else
                    $colors_iterator++;


                echo '<div class="col-md-3 col-sm-6 col-xs-12">';
                echo '<div class="info-box '. $this->colors[$colors_iterator] .'">';
                echo '<span class="info-box-icon"><i class="fa '. $this->icons[$colors_iterator] .'"></i></span>';
                echo '<div class="info-box-content">';
                echo '<span class="info-box-text">'.Html::encode(Yii::t('app',$field)).'</span>';
                echo '<span class="info-box-number">'.Html::encode($item[$field]).'</span>';
                echo '</div></div></div>';
            }

            echo '</dl></div>';

            if($this->displayFooter != false) {

                echo '<div class="timeline-footer" style="text-align: right">';

                echo Html::a(Yii::t('app', 'Zmień'), ['update', 'id' => $item->id], ['class' => 'btn btn-primary btn-md']);

                echo Html::a(Yii::t('app', 'Usuń'), ['delete', 'id' => $item->id], [
                    'id' => 'show-modal',
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure to delete this?',
                        'method' => 'post',
                    ],
                ]);

                echo '</div>';
            }
        }

        echo '</ul>';
    }
    
}
