<?php

use yii\helpers\Html;
use frontend\widgets\Timeline;
use frontend\widgets\Modal;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Study Result');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="study-result-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if(!empty($dataProvider->getModels()))
        echo '<p>'.Html::a(Yii::t('app', 'Create Study Result'), ['create'], ['class' => 'btn btn-success']).'</p>';
    ?>

    <?= Timeline::widget ([

        'dateField' => 'study_date',

        'displayHeader' => true,
        'headerField' => 'id',
        'displayFooter' => true,
        'displayTime' => false,
        'displayIcon' => true,

        'fields' => [
            'erythrocytes',
            'hematocrit',
            'hemoglobin',
            'leukocytes',
            'lymphocytes',
            'pressure',
            'systolic_pressure',
            'diastolic_pressure',
            'pulse'
        ],

        'items' => $dataProvider->getModels(),

    ]); ?>

</div>
