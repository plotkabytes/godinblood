<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\BloodGroup */

$this->title = Yii::t('app', 'Dodaj Blood Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blood Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blood-group-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
