<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\ParsedAmountType */

$this->title = Yii::t('app', 'Zmień {modelClass}: ', [
    'modelClass' => 'Parsed Amount Type',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Parsed Amount Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Zmień');
?>
<div class="parsed-amount-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
