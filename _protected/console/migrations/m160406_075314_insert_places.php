<?php

use yii\db\Migration;

class m160406_075314_insert_places extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%place}}', ['place'], [
          [ 'RCKiK w Białymstoku'],
          [ 'RCKiK w Bydgoszczy'],
          [ 'RCKiK w Gdańsku'],
          [ 'RCKiK w Katowicach'],
          [ 'RCKiK w Kaliszu'],
          [ 'RCKiK w Kielcach'],
          [ 'RCKiK w Krakowie'],
          [ 'RCKiK w Lublinie'],
          [ 'RCKiK w Łodzi'],
          [ 'RCKiK w Olsztynie'],
          [ 'RCKiK w Opolu'],
          [ 'RCKiK w Poznaniu'],
          [ 'RCKiK w Raciborzu'],
          [ 'RCKiK w Radomiu'],
          [ 'RCKiK w Rzeszowie'],
          [ 'RCKiK w Słupsku'],
          [ 'RCKiK w Wałbrzychu'],
          [ 'RCKiK w Warszawie'],
          [ 'RCKiK w Zielonej górze'],
          [ 'RCKiK w Warszawie'],
        ]);
    }

    public function down()
    {
        echo "m160406_075314_insert_places cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
