<?php

use yii\db\Migration;

class m160331_173238_initial_create_table extends Migration
{
  public function up()
  {
      $tableOptions = null;

      if ($this->db->driverName === 'mysql')
      {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%place}}', [
          'id' => $this->primaryKey(),
          'place' => $this->string()->notNull(),
      ], $tableOptions);

      $this->createTable('{{%donation_type}}', [
          'id' => $this->primaryKey(),
          'type' => $this->string()->notNull(),
      ], $tableOptions);

      $this->createTable('{{%blood_group}}', [
          'id' => $this->primaryKey(),
          'group' => $this->string()->notNull(),
      ], $tableOptions);

      $this->createTable('{{%gender}}', [
          'id' => $this->primaryKey(),
          'gender' => $this->string()->notNull(),
      ], $tableOptions);

      $this->createTable('{{%parsed_amount_type}}', [
          'id' => $this->primaryKey(),
          'type' => $this->string()->notNull(),
      ], $tableOptions);
  }

  public function down()
  {
      $this->dropTable('{{%place}}');
      $this->dropTable('{{%donation_type}}');
      $this->dropTable('{{%blood_group}}');
      $this->dropTable('{{%gender}}');
      $this->dropTable('{{%parsed_amount_type}}');
  }

}
