<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\DonationType */

$this->title = Yii::t('app', 'Dodaj Donation Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Donation Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donation-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
