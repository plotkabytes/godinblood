<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\BloodHistory */

$this->title = Yii::t('app', 'Zmień {modelClass}: ', [
    'modelClass' => 'Blood History',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blood Histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Zmień');
?>
<div class="blood-history-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
