<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use frontend\models\BloodGroup;
use frontend\models\Place;
use frontend\models\ParsedAmountType;

/* @var $this yii\web\View */
/* @var $model frontend\models\BloodAmountInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div clas="col-lg-6 col-md-8 col-xs-12">

        <div class="blood-amount-info-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'place_id')->dropDownList(ArrayHelper::map(Place::find()->all(), 'id', 'place')) ?>

            <?= $form->field($model, 'group_id')->dropDownList(ArrayHelper::map(BloodGroup::find()->all(), 'id', 'group')) ?>

            <?= $form->field($model, 'amount_type_id')->dropDownList(ArrayHelper::map(ParsedAmountType::find()->all(), 'id', 'type')) ?>

            <?= $form->field($model, 'parsedat')->widget(\yii\jui\DatePicker::classname(), [
              'language' => 'pl',
              'dateFormat' => 'ddMMyyyy',
              'options' => ['class' => 'form-control']
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Dodaj') : Yii::t('app', 'Zmień'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

    </div>
</div>