<?php

use yii\db\Migration;

class m160409_131302_column_insert_to_personal_info extends Migration
{
    public function up()
    {
      $this->addColumn('{{%personal_info}}','height',$this->integer());
      $this->alterColumn('{{%personal_info}}','gender_id',$this->integer());
    }

    public function down()
    {
      $this->removeColumn('{{%personal_info}}','height',$this->integer());

      return false;
    }

}
