<?php
namespace frontend\controllers;

use frontend\models\AccountActivation;
use frontend\models\ContactForm;
use yii\web\Controller;

use Yii;

/** Odpowiada za zarzadzanie akcjami zwiazanymi z panelem
 * @return string
 */
class PanelController extends Controller //AccessController
{
    public $layout = "panel";

    /** Wyswietla glowna strone panelu
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /** Wylogowanie uzytkownika z panelu
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect('site/index.php');
    }

    /**
     * Wyswietla glowna strone ze statystykami
     */
    public function actionStatistic()
    {
        // for current amounts of blood
        $latest_blood_info  = BloodAmountInfo::find()->max('parsedat')->all();

        // for displaying latest blood study result
        $latest_blood_study = StudyResult::find()->max('study_date')->one();

        // for displaying latest blood declaration
        $lastest_blood_declaration = BloodDeclaration::find()->max('id')->one();

        // for calculating blood given to blood centers
        $given_blood = BloodHistory::find()->max('amount')->all();

        $this->render('statistic', [
            'latest_blood_info' => $latest_blood_info,
            'latest_blood_study' => $latest_blood_study,
            'lastest_blood_declaration' => $lastest_blood_declaration,
            'given_blood' => $given_blood
        ]);
    }

}
