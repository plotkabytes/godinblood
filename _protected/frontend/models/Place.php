<?php

namespace frontend\models;

use Yii;
use yii\web\Link;
use yii\web\Linkable;
use yii\helpers\Url;

/**
 * This is the model class for table "place".
 *
 * @property integer $id
 * @property string $place
 *
 * @property BloodAmountInfo[] $bloodAmountInfos
 * @property BloodDeclaration[] $bloodDeclarations
 * @property BloodHistory[] $bloodHistories
 * @property BloodRequest[] $bloodRequests
 */
class Place extends \yii\db\ActiveRecord implements Linkable
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'place';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['place'], 'required'],
            [['place'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'place' => Yii::t('app', 'Place'),
        ];
    }

    /**
     *  Filter fields for the REST API
     *  @return $fields
     */
    public function fields()
    {
        $fields = parent::fields();

        unset($fields['id']);

        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBloodAmountInfos()
    {
        return $this->hasMany(BloodAmountInfo::className(), ['place_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBloodDeclarations()
    {
        return $this->hasMany(BloodDeclaration::className(), ['place_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBloodHistories()
    {
        return $this->hasMany(BloodHistory::className(), ['place_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBloodRequests()
    {
        return $this->hasMany(BloodRequest::className(), ['place_id' => 'id']);
    }

    /**
     * @return REST link
     */
    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['place/view', 'id' => $this->id], true),
        ];
    }

}
