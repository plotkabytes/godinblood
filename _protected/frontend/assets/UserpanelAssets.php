<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use Yii;

Yii::setAlias('@themes', Yii::$app->view->theme->baseUrl);

class UserpanelAssets extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@themes';
    public $sourcePath = '@bower/';

    public $css = [
        'UserPanel/css/AdminLTE.css',
        'UserPanel/css/panel-styles.css',
        'UserPanel/css/skin-red.css',
        'UserPanel/css/pace.min.css',
        '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css',
        '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css',
    ];
    public $js = [
        'UserPanel/js/site.js',
        'UserPanel/js/app.js',
        'UserPanel/js/pace.js'
        //'UserPanel/js/dashboard.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}
