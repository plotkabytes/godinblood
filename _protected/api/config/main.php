<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module'
        ]
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@api/runtime/logs/error.log'
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                  'class' => 'yii\rest\UrlRule',
                  'controller' => 'v1/bloodgroup',
                  'pluralize' => false,
                  'extraPatterns' => [
                        'GET bloodgroup' => 'bloodgroup'
                    ],
                ],
                [
                  'class' => 'yii\rest\UrlRule',
                  'controller' => 'v1/donationtype',
                  'pluralize' => false
                ],
                [
                  'class' => 'yii\rest\UrlRule',
                  'controller' => 'v1/blooddeclaration',
                  'pluralize' => false
                ],
                [
                  'class' => 'yii\rest\UrlRule',
                  'controller' => 'v1/bloodhistory',
                  'pluralize' => false
                ],
                [
                  'class' => 'yii\rest\UrlRule',
                  'controller' => 'v1/bloodrequest',
                  'pluralize' => false
                ],
                [
                  'class' => 'yii\rest\UrlRule',
                  'controller' => 'v1/bloodrequestanswers',
                  'pluralize' => false
                ],
                [
                  'class' => 'yii\rest\UrlRule',
                  'controller' => 'v1/gender',
                  'pluralize' => false
                ],
                [
                  'class' => 'yii\rest\UrlRule',
                  'controller' => 'v1/bloodamountinfo',
                  'pluralize' => false
                ],
                [
                  'class' => 'yii\rest\UrlRule',
                  'controller' => 'v1/parsedamounttype',
                  'pluralize' => false
                ],
                [
                  'class' => 'yii\rest\UrlRule',
                  'controller' => 'v1/personalinfo',
                  'pluralize' => false
                ],
                [
                  'class' => 'yii\rest\UrlRule',
                  'controller' => 'v1/place',
                  'pluralize' => false
                ],
                [
                  'class' => 'yii\rest\UrlRule',
                  'controller' => 'v1/studyresult',
                  'pluralize' => false
                ],
                [
                  'class' => 'yii\rest\UrlRule',
                  'controller' => 'v1/auth',
                  'pluralize' => false
                ],
            ],
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'response' => [
            'format' =>  \yii\web\Response::FORMAT_JSON,
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                if ($response->data !== null && Yii::$app->request->get('suppress_response_code')) {
                    $response->data = [
                        'success' => $response->isSuccessful,
                        'data' => $response->data,
                    ];
                    $response->statusCode = 200;
                }
            },
        ],
    ],
    'params' => $params,
];
