<?php
namespace frontend\widgets;

use yii;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Modal widget.
 */
class Modal extends Widget
{

    public $name;
    public $id;
    public $message;
    public $colorClass;
    public $buttons;
    public $url;

    public function init()
    {
        parent::init();

        if($this->name === null) {
            $this->name = 'Modal';
        }

        if($this->url === null) {
            $this->url = '';
        }

        if($this->id === null) {
            $this->id = 'myModal';
        }

        if($this->message === null) {
            $this->message = 'Modal message';
        }

        if($this->colorClass === null) {
            $this->colorClass = 'Modal';
        }

        if($this->buttons === null) {
            $this->buttons = array('Delete' => 'smth');
        }

    }

    public function run()
    {
        echo '<div id="'.$this->id.'" class="modal '.$this->colorClass.'">';
        echo '<div class="modal-dialog">';
        echo '<div class="modal-content">';
        echo '<div class="modal-header">';
        echo '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
        echo '<h4 class="modal-title">'.$this->name.'</h4>';
        echo '</div>';
        echo '<div class="modal-body">';
        echo '<p>'.$this->message.'</p>';
        echo '</div>';
        echo '<div class="modal-footer">';

        foreach ($this->buttons as $button => $action)
        {

            /* need js to get ID! */
            echo Html::a(Yii::t('app', 'Usuń'), ['delete'], [
                'id' => 'delete-modal',
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure to delete this?',
                    'method' => 'post',
                ],
            ]);

        }
        echo '</div>';
        echo '</div>';
        echo '</div>';
        echo '</div>';
    }
}


