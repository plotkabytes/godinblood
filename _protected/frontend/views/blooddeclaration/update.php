<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\BloodDeclaration */

$this->title = Yii::t('app', 'Zmień {modelClass}: ', [
    'modelClass' => 'Blood Declaration',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blood Declarations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Zmień');
?>
<div class="blood-declaration-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
