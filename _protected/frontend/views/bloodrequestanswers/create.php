<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\BloodRequestAnswers */

$this->title = Yii::t('app', 'Dodaj Blood Request Answers');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blood Request Answers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blood-request-answers-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
