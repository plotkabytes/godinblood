<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\BloodAmountInfo */

$this->title = Yii::t('app', 'Dodaj Blood Amount Info');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blood Amount Infos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blood-amount-info-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
