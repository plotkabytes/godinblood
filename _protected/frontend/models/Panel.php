<?php

namespace frontend\models;

use Yii;

class Panel extends \yii\base\Model
{

    /** get map from location parameters
     * @param $height - wysokosc mapy
     * @param $width - dlugosc mapy
     * @param $locX - lokalizacja, dlugosc
     * @param $locY - lokalizacja, szerokosc
     * @param $zoom - powiekszenie
     * @return string zwraca html mapy
     */
    public static function getMap($height, $width, $locX, $locY, $zoom)
    {
        return '<iframe frameborder="0" style="border:0" src="https://www.google.com/maps/d/embed?mid=zRE8hyxh-ifQ.k7u7sfIllW5k&z='.$zoom.'&ll='.$locX.','.$locY.'" width="'.$width.'" height="'.$height.'"></iframe>';
    }

    public static function getAmounts()
    {
        return
        '<table>
		<thead>
		<tr>
			<th class="rotate"><div><span>Grupa krwi</span></div></th>
			<th class="rotate"><div><span>Białystok</span></div></th>
			<th class="rotate"><div><span>Bydgoszcz</span></div></th>
			<th class="rotate"><div><span>Gdańsk</span></div></th>
			<th class="rotate"><div><span>Kalisz</span></div></th>
			<th class="rotate"><div><span>Katowice</span></div></th>
			<th class="rotate"><div><span>Kielce</span></div></th>
			<th class="rotate"><div><span>Kraków</span></div></th>
			<th class="rotate"><div><span>Lublin</span></div></th>
			<th class="rotate"><div><span>Łódź</span></div></th>
			<th class="rotate"><div><span>Olsztyn</span></div></th>
			<th class="rotate"><div><span>Opole</span></div></th>
			<th class="rotate"><div><span>Poznań</span></div></th>
			<th class="rotate"><div><span>Racibórz</span></div></th>
			<th class="rotate"><div><span>Radom</span></div></th>
			<th class="rotate"><div><span>Rzeszów</span></div></th>
			<th class="rotate"><div><span>Słupsk</span></div></th>
			<th class="rotate"><div><span>Szczecin</span></div></th>
			<th class="rotate"><div><span>Wałbrzych</span></div></th>
			<th class="rotate"><div><span>Warszawa</span></div></th>
			<th class="rotate"><div><span>Wrocław</span></div></th>
			<th class="rotate"><div><span>Zielona Góra</span></div></th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><h3>0 Rh-</h3></td>
			<td><img src="/images/krew2.png" alt="Białystok"></td>
			<td><img src="/images/krew3.png" alt="Bydgoszcz"></td>
			<td><img src="/images/krew3.png" alt="Gdańsk"></td>
			<td><img src="/images/krew2.png" alt="Kalisz"></td>
			<td><img src="/images/krew2.png" alt="Katowice"></td>
			<td><img src="/images/krew3.png" alt="Kielce"></td>
			<td><img src="/images/krew4.png" alt="Kraków"></td>
			<td><img src="/images/krew3.png" alt="Lublin"></td>
			<td><img src="/images/krew4.png" alt="Łódź"></td>
			<td><img src="/images/krew3.png" alt="Olsztyn"></td>
			<td><img src="/images/krew2.png" alt="Opole"></td>
			<td><img src="/images/krew2.png" alt="Poznań"></td>
			<td><img src="/images/krew4.png" alt="Racibórz"></td>
			<td><img src="/images/krew3.png" alt="Radom"></td>
			<td><img src="/images/krew2.png" alt="Rzeszów"></td>
			<td><img src="/images/krew2.png" alt="Słupsk"></td>
			<td><img src="/images/krew2.png" alt="Szczecin"></td>
			<td><img src="/images/krew3.png" alt="Wałbrzych"></td>
			<td><img src="/images/krew3.png" alt="Warszawa"></td>
			<td><img src="/images/krew3.png" alt="Wrocław"></td>
			<td><img src="/images/krew3.png" alt="Zielona Góra"></td>
		</tr>
		<tr>
			<td><h3>0 Rh+</h3><h3></h3></td>
			<td><img src="/images/krew2.png" alt="Białystok"></td>
			<td><img src="/images/krew2.png" alt="Bydgoszcz"></td>
			<td><img src="/images/krew2.png" alt="Gdańsk"></td>
			<td><img src="/images/krew1.png" alt="Kalisz"></td>
			<td><img src="/images/krew1.png" alt="Katowice"></td>
			<td><img src="/images/krew3.png" alt="Kielce"></td>
			<td><img src="/images/krew4.png" alt="Kraków"></td>
			<td><img src="/images/krew2.png" alt="Lublin"></td>
			<td><img src="/images/krew4.png" alt="Łódź"></td>
			<td><img src="/images/krew3.png" alt="Olsztyn"></td>
			<td><img src="/images/krew1.png" alt="Opole"></td>
			<td><img src="/images/krew1.png" alt="Poznań"></td>
			<td><img src="/images/krew4.png" alt="Racibórz"></td>
			<td><img src="/images/krew3.png" alt="Radom"></td>
			<td><img src="/images/krew2.png" alt="Rzeszów"></td>
			<td><img src="/images/krew2.png" alt="Słupsk"></td>
			<td><img src="/images/krew1.png" alt="Szczecin"></td>
			<td><img src="/images/krew3.png" alt="Wałbrzych"></td>
			<td><img src="/images/krew3.png" alt="Warszawa"></td>
			<td><img src="/images/krew1.png" alt="Wrocław"></td>
			<td><img src="/images/krew1.png" alt="Zielona Góra"></td>
		</tr>
		<tr>
			<td><h3>A Rh-</h3></td>
			<td><img src="/images/krew2.png" alt="Białystok"></td>
			<td><img src="/images/krew2.png" alt="Bydgoszcz"></td>
			<td><img src="/images/krew3.png" alt="Gdańsk"></td>
			<td><img src="/images/krew2.png" alt="Kalisz"></td>
			<td><img src="/images/krew1.png" alt="Katowice"></td>
			<td><img src="/images/krew3.png" alt="Kielce"></td>
			<td><img src="/images/krew4.png" alt="Kraków"></td>
			<td><img src="/images/krew3.png" alt="Lublin"></td>
			<td><img src="/images/krew4.png" alt="Łódź"></td>
			<td><img src="/images/krew3.png" alt="Olsztyn"></td>
			<td><img src="/images/krew2.png" alt="Opole"></td>
			<td><img src="/images/krew2.png" alt="Poznań"></td>
			<td><img src="/images/krew4.png" alt="Racibórz"></td>
			<td><img src="/images/krew3.png" alt="Radom"></td>
			<td><img src="/images/krew3.png" alt="Rzeszów"></td>
			<td><img src="/images/krew2.png" alt="Słupsk"></td>
			<td><img src="/images/krew2.png" alt="Szczecin"></td>
			<td><img src="/images/krew3.png" alt="Wałbrzych"></td>
			<td><img src="/images/krew3.png" alt="Warszawa"></td>
			<td><img src="/images/krew2.png" alt="Wrocław"></td>
			<td><img src="/images/krew2.png" alt="Zielona Góra"></td>
		</tr>
		<tr>
			<td><h3>A Rh+</h3></td>
			<td><img src="/images/krew2.png" alt="Białystok"></td>
			<td><img src="/images/krew2.png" alt="Bydgoszcz"></td>
			<td><img src="/images/krew1.png" alt="Gdańsk"></td>
			<td><img src="/images/krew1.png" alt="Kalisz"></td>
			<td><img src="/images/krew1.png" alt="Katowice"></td>
			<td><img src="/images/krew3.png" alt="Kielce"></td>
			<td><img src="/images/krew4.png" alt="Kraków"></td>
			<td><img src="/images/krew2.png" alt="Lublin"></td>
			<td><img src="/images/krew4.png" alt="Łódź"></td>
			<td><img src="/images/krew3.png" alt="Olsztyn"></td>
			<td><img src="/images/krew2.png" alt="Opole"></td>
			<td><img src="/images/krew1.png" alt="Poznań"></td>
			<td><img src="/images/krew4.png" alt="Racibórz"></td>
			<td><img src="/images/krew3.png" alt="Radom"></td>
			<td><img src="/images/krew2.png" alt="Rzeszów"></td>
			<td><img src="/images/krew2.png" alt="Słupsk"></td>
			<td><img src="/images/krew1.png" alt="Szczecin"></td>
			<td><img src="/images/krew3.png" alt="Wałbrzych"></td>
			<td><img src="/images/krew2.png" alt="Warszawa"></td>
			<td><img src="/images/krew2.png" alt="Wrocław"></td>
			<td><img src="/images/krew2.png" alt="Zielona Góra"></td>
		</tr>
		<tr>
			<td><h3>B Rh-</h3></td>
			<td><img src="/images/krew2.png" alt="Białystok"></td>
			<td><img src="/images/krew3.png" alt="Bydgoszcz"></td>
			<td><img src="/images/krew3.png" alt="Gdańsk"></td>
			<td><img src="/images/krew2.png" alt="Kalisz"></td>
			<td><img src="/images/krew2.png" alt="Katowice"></td>
			<td><img src="/images/krew3.png" alt="Kielce"></td>
			<td><img src="/images/krew4.png" alt="Kraków"></td>
			<td><img src="/images/krew3.png" alt="Lublin"></td>
			<td><img src="/images/krew4.png" alt="Łódź"></td>
			<td><img src="/images/krew3.png" alt="Olsztyn"></td>
			<td><img src="/images/krew2.png" alt="Opole"></td>
			<td><img src="/images/krew2.png" alt="Poznań"></td>
			<td><img src="/images/krew4.png" alt="Racibórz"></td>
			<td><img src="/images/krew3.png" alt="Radom"></td>
			<td><img src="/images/krew2.png" alt="Rzeszów"></td>
			<td><img src="/images/krew1.png" alt="Słupsk"></td>
			<td><img src="/images/krew2.png" alt="Szczecin"></td>
			<td><img src="/images/krew3.png" alt="Wałbrzych"></td>
			<td><img src="/images/krew3.png" alt="Warszawa"></td>
			<td><img src="/images/krew2.png" alt="Wrocław"></td>
			<td><img src="/images/krew3.png" alt="Zielona Góra"></td>
		</tr>
		<tr>
			<td><h3>B Rh+</h3></td>
			<td><img src="/images/krew1.png" alt="Białystok"></td>
			<td><img src="/images/krew2.png" alt="Bydgoszcz"></td>
			<td><img src="/images/krew1.png" alt="Gdańsk"></td>
			<td><img src="/images/krew0.png" alt="Kalisz"></td>
			<td><img src="/images/krew1.png" alt="Katowice"></td>
			<td><img src="/images/krew3.png" alt="Kielce"></td>
			<td><img src="/images/krew4.png" alt="Kraków"></td>
			<td><img src="/images/krew1.png" alt="Lublin"></td>
			<td><img src="/images/krew4.png" alt="Łódź"></td>
			<td><img src="/images/krew1.png" alt="Olsztyn"></td>
			<td><img src="/images/krew1.png" alt="Opole"></td>
			<td><img src="/images/krew2.png" alt="Poznań"></td>
			<td><img src="/images/krew4.png" alt="Racibórz"></td>
			<td><img src="/images/krew2.png" alt="Radom"></td>
			<td><img src="/images/krew1.png" alt="Rzeszów"></td>
			<td><img src="/images/krew1.png" alt="Słupsk"></td>
			<td><img src="/images/krew1.png" alt="Szczecin"></td>
			<td><img src="/images/krew2.png" alt="Wałbrzych"></td>
			<td><img src="/images/krew1.png" alt="Warszawa"></td>
			<td><img src="/images/krew1.png" alt="Wrocław"></td>
			<td><img src="/images/krew2.png" alt="Zielona Góra"></td>
		</tr>
		<tr>
			<td><h3>AB Rh-</h3></td>
			<td><img src="/images/krew1.png" alt="Białystok"></td>
			<td><img src="/images/krew3.png" alt="Bydgoszcz"></td>
			<td><img src="/images/krew3.png" alt="Gdańsk"></td>
			<td><img src="/images/krew2.png" alt="Kalisz"></td>
			<td><img src="/images/krew2.png" alt="Katowice"></td>
			<td><img src="/images/krew3.png" alt="Kielce"></td>
			<td><img src="/images/krew4.png" alt="Kraków"></td>
			<td><img src="/images/krew3.png" alt="Lublin"></td>
			<td><img src="/images/krew4.png" alt="Łódź"></td>
			<td><img src="/images/krew3.png" alt="Olsztyn"></td>
			<td><img src="/images/krew2.png" alt="Opole"></td>
			<td><img src="/images/krew1.png" alt="Poznań"></td>
			<td><img src="/images/krew4.png" alt="Racibórz"></td>
			<td><img src="/images/krew3.png" alt="Radom"></td>
			<td><img src="/images/krew3.png" alt="Rzeszów"></td>
			<td><img src="/images/krew2.png" alt="Słupsk"></td>
			<td><img src="/images/krew2.png" alt="Szczecin"></td>
			<td><img src="/images/krew1.png" alt="Wałbrzych"></td>
			<td><img src="/images/krew3.png" alt="Warszawa"></td>
			<td><img src="/images/krew1.png" alt="Wrocław"></td>
			<td><img src="/images/krew3.png" alt="Zielona Góra"></td>
		</tr>
		<tr>
			<td><h3>AB Rh+</h3></td>
			<td><img src="/images/krew1.png" alt="Białystok"></td>
			<td><img src="/images/krew2.png" alt="Bydgoszcz"></td>
			<td><img src="/images/krew1.png" alt="Gdańsk"></td>
			<td><img src="/images/krew0.png" alt="Kalisz"></td>
			<td><img src="/images/krew1.png" alt="Katowice"></td>
			<td><img src="/images/krew3.png" alt="Kielce"></td>
			<td><img src="/images/krew4.png" alt="Kraków"></td>
			<td><img src="/images/krew1.png" alt="Lublin"></td>
			<td><img src="/images/krew4.png" alt="Łódź"></td>
			<td><img src="/images/krew1.png" alt="Olsztyn"></td>
			<td><img src="/images/krew2.png" alt="Opole"></td>
			<td><img src="/images/krew1.png" alt="Poznań"></td>
			<td><img src="/images/krew4.png" alt="Racibórz"></td>
			<td><img src="/images/krew3.png" alt="Radom"></td>
			<td><img src="/images/krew1.png" alt="Rzeszów"></td>
			<td><img src="/images/krew1.png" alt="Słupsk"></td>
			<td><img src="/images/krew1.png" alt="Szczecin"></td>
			<td><img src="/images/krew1.png" alt="Wałbrzych"></td>
			<td><img src="/images/krew2.png" alt="Warszawa"></td>
			<td><img src="/images/krew1.png" alt="Wrocław"></td>
			<td><img src="/images/krew3.png" alt="Zielona Góra"></td>
		</tr>
		</tbody>
	</table>';
    }

}
