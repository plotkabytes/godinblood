<?php

namespace api\modules\v1\controllers;

use yii\filters\auth\HttpBearerAuth;
use yii\web\ForbiddenHttpException;
use yii\rest\ActiveController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;
use frontend\models\BloodAmountInfo;
use common\models\User;
use Yii;

/**
 * BloodamountinfoController is class used to
 * get info about amount of blood in specific place and specific blood group
 * @author Mateusz Żyła
 */
class BloodamountinfoController extends ActiveController
{
    public $modelClass = 'frontend\models\BloodAmountInfo';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }

    /**
     * Get all data from blood amount info table and send it to the user
     *
     * ```
     * HEADER: Authorization : Bearer $token
     * GET http://bloodgang.local/api/v1/bloodamountinfo
     * ```
     *
     * @return JSON with $info
     */
    public function actionIndex()
    {
        $info = BloodAmountInfo::find()->asArray()->orderBy('parsedat DESC')->all();

        return $info;
    }

}
