<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\widgets\Accordion;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Blood History');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blood-history-index">


    <?= Accordion::widget ([

        'mainTitle' => 'Historia Donacji',

        'items' => $dataProvider->getModels(),

    ]); ?>

</div>
