<?php

return [

    //-- general buttons --//
    'Submit' => 'Zatwierdź',
    'Cancel' => 'Anuluj',
    'Update' => 'Modyfikuj',
    'Delete' => 'Usuń',
    'Create' => 'Dodaj',
    'Send' => 'Wyślij',
    'Save' => 'Zapisz',
    'Menu' => 'Menu',
    'Back' => 'Powrót',

    //-- CMS buttons --//
    'Create User' => 'Dodaj użytkownika',
    'Create Article' => 'Dodaj artykuł',

    //-- top menu --//
    'Home' => 'Główna',
    'About' => 'O nas',
    'Contact' => 'Kontakt',
    'Login' => 'Zaloguj',
    'Logout' => 'Wyloguj',
    'Users' => 'Użytkownicy',
    'Articles' => 'Artykuły',
    'Register' => 'Rejestracja',

    //-- static pages --//
    'Password' => 'Hasło',
    'Username' => 'Nazwa użytkownika',

    // contact
    'Name' => 'Imię',
    'Subject' => 'Temat',
    'Text' => 'Treść',
    'Verification Code' => 'Kod weryfikacyjny',
    'If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.'
    => 'Jeśli masz jakieś pytania do nas, prosimy o wysłanie maila korzystając z zamieszczonego tutaj formularza.',
    'Thank you for contacting us. We will respond to you as soon as possible.' => 'Dziękujemy za wiadomość. Odpowiemy tak szybko jak będzie to możliwe',
    'There was an error sending email.' => 'Wystąpił błąd podczas wysyłania wiadomości, spróbuj ponownie później.',

    // password reset
    'If you forgot your password you can' => 'Jeśli zapomnisz hasła możesz ',
    'reset it' => 'je zresetować',
    'Request password reset' => 'Zresetuj hasło',
    'Please fill out your email.' => 'Proszę podać email.',
    'Reset password' => 'Resetuj hasło',
    'A link to reset password will be sent to your email.' => 'Email z linkiem resetującym hasło został wysłany na podany adres email.',
    'Check your email for further instructions.' => 'Sprawdź swój email aby przejść do następnego kroku.',
    'Please choose your new password:' => 'Proszę podać nowe hasło:',
    'New password was saved.' => 'Nowe hasło zostało zapisane.',
    'Sorry, we are unable to reset password for email provided.' => 'Nie możemy zresetować hasła dla podanego adresu email.',
    'Password reset token cannot be blank.' => 'Pole token nie może być puste.',
    'Wrong password reset token.' => 'Wprowadzono zły token.',

    // signup
    'Signup' => 'Rejestracja',
    'Please fill out the following fields to signup:' => 'Proszę wypełnić podane pola aby się zarejestrować:',
    'We will send you an email with account activation link.' => 'Za chwilę wyślemy email z linkiem aktywacyjnym do Twojego konta. ',
    'We couldn\'t sign you up, please contact us.' => 'Rejestracja nie była możliwa. Prosimy o kontakt.',
    'If you dont have account you can' => 'Jeśli nie posiadasz konta mozesz ',
    'create it' => 'je załozyć',

    // login
    'Remember me' => 'Zapamiętaj mnie',
    'Please fill out the following fields to login:' => 'Proszę wypełnić podane pola aby się zalogować:',
    'You have to activate your account first. Please check your email.' => 'Zanim będzie można się zalogować trzeba aktywować konto. Proszę sprawdzić swoją skrzynkę email',
    'To be able to log in, you need to confirm your registration. Please check your email, we have sent you a message.'
    => 'Zanim będzie można się zalogować trzeba aktywować konto. Proszę sprawdzić swoją skrzynkę email.',

    // account activation
    'We couldn\'t send you account activation email, please contact us.' => 'Nie mogliśmy wysłać maila aktywacyjnego. Prosimy o kontakt.',
    'Success! You can now log in.' => 'Gratulacje! Operacja przebiegła prawidłowo. Można się teraz zalogować.',
    'for joining us!' => 'za dołączenie do nas!',
    'your account could not be activated, please contact us!' => 'Coś poszło nie tak podczas aktywacji konta. Prosimy o kontakt',
    'Account activation token cannot be blank.' => 'Token aktywacyjny nie może być pusty.',
    'Wrong account activation token.' => 'Token aktywacyjny jest nie prawidłowy.',

    //-- general db fields --//
    'Created At' => 'Utworzono',
    'Updated At' => 'Zmodyfikowano',

    //-- mixed --//
    'My Company' => 'Moja Firma',
    'Hello' => 'Witaj',
    'Thank you' => 'Dziękujemy',

    //-- users management --//
    'Role' => 'Rola',
    'Create User' => 'Dodaj użytkownika',
    'Update User' => 'Modyfikuj użytkownika',
    'New pwd ( if you want to change it )' => 'Nowe hasło (nie obowiązkowe!)',

    //-- articles --//
    'Title' => 'Tytuł',
    'Summary' => 'Podsumowanie',
    'Content' => 'Zawartość',
    'Status' => 'Status',
    'Category' => 'Kategoria',
    'Author' => 'Autor',
    'articles' => 'Artykuły',
    'news' => 'Newsy',
    'The best news available' => 'Najlepsze newsy',
    'We haven\'t created any articles yet.' => 'Nie mamy żadnych dodanych artykułow.',
    'Read more' => 'Więcej',
    'Published on' => 'Opublikowano dnia',

    // statuses
    'Draft' => 'Kopia robocza',
    'Published' => 'Opublikowano',
    'Archived' => 'Zarchiwizowano',

    // categories
    'Economy' => 'Ekonomia',
    'Society' => 'Społeczeństwo',
    'Sport' => 'Sport',

    //-- errors --//
    'The above error occurred while the Web server was processing your request.' => 'Wystąpił błąd podczas przetwarzania Twojego żądania.',
    'Please contact us if you think this is a server error. Thank you.' => 'Jeśli uważasz, że jest to błąd serwera, prosimy o kontakt. Dziękujemy!',
    'You are not allowed to access this page.' => 'Nie masz wystarczających uprawniń do przeglądania tej strony.',

    //-- delete messages --//
    'Are you sure you want to delete this user?' => 'Jesteś pewny, że chcesz usunąć tego użytkownika?',
    'Are you sure you want to delete this article?' => 'Jesteś pewny, że chcesz usunąć ten artykuł?',

    //-- models --//
    'Place ID' => 'Miejsce',
    'Group ID' => 'Grupa Krwii',
    'Amount Type ID' => 'Pobrany typ',
    'Parsedat' => 'Data Parsowania',
    'User ID' => 'Użytkownik',
    'Donation Type ID' => 'Typ donacji',
    'Group' => 'Grupa krwii',
    'Type ID' => 'Typ oddanej krwii',
    'Amount' => 'Ilosc',
    'Reason' => 'Powod odmowy',
    'For Who' => 'Dla kogo',
    'Description' => 'Opis',
    'Request ID' => 'Żądanie',
    'Type' => 'Typ donacji',
    'Gender' => 'Płeć',
    'Parsed Type' => 'Pobrana ilość',
    'Gender ID' => 'Płeć',
    'Weight' => 'Waga',
    'Height' => 'Wzrost',
    'Place' => 'Miejsce',
    'Erythrocytes' => 'Erytrocyty',
    'Hematocrit' => 'Hematokryt',
    'Hemoglobin' => 'Hemoglobina',
    'Leukocytes' => 'Leukocyty',
    'Lymphocytes' => 'Limfocyty',
    'Pressure' => 'Ciśnienie',
    'Systolic Pressure' => 'Ciśnienie skurczowe',
    'Diastolic Pressure' => 'Cisnienie rozkurczowe',
    'Pulse' => 'Puls',
    'Study Date' => 'Data badania',
    'Logged as ' => 'Zalogowany jako ',
    'You have no items to display' => 'Brak elementow do wyświetlenia',
    'Maybe you want to' => 'Byc moze chcesz',
    'create one?' => 'cos dodac?',
    'Study Result' => 'Wyniki badań',
    'Studies' => 'Wyniki badań',
    'Declarations' => 'Deklaracje',
    'Requests' => 'Prośby',
    'Statistic' => 'Statystki',
    'Overall Requests' => 'Wszystkie prośby',
    'My Requests' => 'Moje prośby',
    'My Answers' => 'Moje odpowiedzi',
    'Search...' => 'Wyszukaj...',
    'erythrocytes' => 'Erytrocyty',
    'hematocrit' => 'Hematokryt',
    'hemoglobin' => 'Hemoglobina',
    'leukocytes' => 'Leukocyty',
    'lymphocytes' => 'Limfocyty',
    'pressure' => 'Ciśnienie',
    'systolic_pressure' => 'Ciśnienie skórczowe',
    'diastolic_pressure' => 'Ciśnienie rozkurczowe',
    'pulse' => 'Puls',
    'BloodHistory' => 'Historia donacji',
    'Blood History' => 'Historia donacji',
    'Create Study Result' => 'Dodaj wynik badania',
    'Create Blood History' => 'Dodaj donacje'
];
