<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\BloodRequestAnswers */

$this->title = Yii::t('app', 'Zmień {modelClass}: ', [
    'modelClass' => 'Blood Request Answers',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blood Request Answers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Zmień');
?>
<div class="blood-request-answers-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
