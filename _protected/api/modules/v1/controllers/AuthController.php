<?php

namespace api\modules\v1\controllers;

use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\models\User;
use Yii;

/**
 * AuthController is default class that is used to get Authorization Token.
 *
 * @author Mateusz Żyła
 */
class AuthController extends ActiveController
{
    public $modelClass = 'common\models\User';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        unset($actions['view']);
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);

        return $actions;
    }

    /**
     * Validate User credentials and returns access token.
     * Pass only via HTTPS !
     *
     * ```
     * HEADER: Authorization : Bearer $token
     * BODY: email : $email, username : $username, password : $password
     * GET http://bloodgang.local/api/v1/auth
     * ```
     *
     * @return JSON with $token or error
     */
    public function actionIndex()
    {
        $lwe = Yii::$app->params['lwe'];
        $request = Yii::$app->getRequest();

        $user_login = $lwe ? $request->getBodyParam('email') : $request->getBodyParam('username');
        $user_password = $request->getBodyParam('password');

        $user = $lwe ? User::findByEmail($user_login) : User::findByUsername($user_login);

        if($user === null)
        {
          return 'Incorrect credentials or user is inactive';
        }

        $result = ($user->validatePassword($user_password)) ? $user->getAuthKey() : ['result' => 'Incorrect password'];

        return $result;
    }

}
