<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Gender */

$this->title = Yii::t('app', 'Zmień {modelClass}: ', [
    'modelClass' => 'Gender',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Genders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Zmień');
?>
<div class="gender-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
