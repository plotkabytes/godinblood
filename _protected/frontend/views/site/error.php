<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;


?>
<div class="error-container">
    <div class="error-body">

        <div class="error-title">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>

        <div class="alert alert-danger">
            <?= nl2br(Html::encode($message)) ?>
        </div>

        <div class="row">
            <div class="error-bottom">
                <p>
                    <?= Yii::t('app', 'The above error occurred while the Web server was processing your request.') ?>
                </p>
                <p>
                    <?= Yii::t('app', 'Please contact us if you think this is a server error. Thank you.') ?>
                </p>

                <div class="form-group text-center">
                    <br>
                    <?= Html::a(Yii::t('app', 'Back'), ['site/login'], ['class' => 'btn back-button', 'name' => 'back-button']) ?>
                </div>
            </div>
        </div>


    </div>
</div>
