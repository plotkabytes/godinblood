<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use Yii;

Yii::setAlias('@themes', Yii::$app->view->theme->baseUrl);

class MainsiteAssets extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@themes';
    public $sourcePath = '@bower/';

    public $css = [
        'MainSite/css/site.css',
    ];
    public $js = [
        'MainSite/js/app.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}
