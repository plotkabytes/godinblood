<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\BloodHistory */

$this->title = Yii::t('app', 'Dodaj Blood History');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blood Histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blood-history-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
