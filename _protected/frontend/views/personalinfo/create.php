<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\PersonalInfo */

$this->title = Yii::t('app', 'Dodaj Personal Info');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Personal Infos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personal-info-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
