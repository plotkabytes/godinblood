<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\BloodRequest */

$this->title = Yii::t('app', 'Dodaj Blood Request');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Blood Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blood-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
