<?php

use yii\db\Migration;

class m160331_182249_default_data_insertion extends Migration
{
    public function up()
    {
      /* insert genders */
      $this->insert('{{%gender}}',[
        'gender' => 'meżczyzna'
      ]);

      $this->insert('{{%gender}}',[
        'gender' => 'kobieta'
      ]);

      $this->insert('{{%blood_group}}', [
        'group' => '0 Rh-'
      ]);

      $this->insert('{{%blood_group}}', [
        'group' => '0 Rh+'
      ]);

      $this->insert('{{%blood_group}}', [
        'group' => 'A Rh-'
      ]);

      $this->insert('{{%blood_group}}', [
        'group' => 'A Rh+'
      ]);

      $this->insert('{{%blood_group}}', [
        'group' => 'B Rh-'
      ]);

      $this->insert('{{%blood_group}}', [
        'group' => 'B Rh+'
      ]);

      $this->insert('{{%blood_group}}', [
        'group' => 'AB Rh-'
      ]);

      $this->insert('{{%blood_group}}', [
        'group' => 'AB Rh+'
      ]);

      $this->insert('{{%parsed_amount_type}}', [
        'type' => 'to_high'
      ]);

      $this->insert('{{%parsed_amount_type}}', [
        'type' => 'high'
      ]);

      $this->insert('{{%parsed_amount_type}}', [
        'type' => 'average'
      ]);

      $this->insert('{{%parsed_amount_type}}', [
        'type' => 'low'
      ]);

      $this->insert('{{%parsed_amount_type}}', [
        'type' => 'no_data'
      ]);

      $this->insert('{{%donation_type}}', [
        'type' => 'Krew pełna'
      ]);

      $this->insert('{{%donation_type}}', [
        'type' => 'Krwinki białe'
      ]);

      $this->insert('{{%donation_type}}', [
        'type' => 'Krwinki czerwone'
      ]);

      $this->insert('{{%donation_type}}', [
        'type' => 'Osocze'
      ]);

      $this->insert('{{%donation_type}}', [
        'type' => 'Trombocyty'
      ]);

    }

    public function down()
    {
        echo "m160331_182249_default_data_insertion cannot be reverted.\n";

        return false;
    }
}
