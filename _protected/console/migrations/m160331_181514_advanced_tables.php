<?php

use yii\db\Migration;

class m160331_181514_advanced_tables extends Migration
{
  public function up()
  {
      $tableOptions = null;

      if ($this->db->driverName === 'mysql')
      {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%personal_info}}', [
          'id' => $this->primaryKey(),
          'user_id' => $this->integer()->notNull(),
          'gender_id' => $this->integer(),
          'weight' => $this->integer(),
          'FOREIGN KEY (user_id) REFERENCES {{%user}}(id)
              ON DELETE CASCADE ON UPDATE CASCADE',
          'FOREIGN KEY (gender_id) REFERENCES {{%gender}}(id)
              ON DELETE CASCADE ON UPDATE CASCADE',
      ], $tableOptions);

      $this->createTable('{{%blood_history}}', [
          'id' => $this->primaryKey(),
          'user_id' => $this->integer()->notNull(),
          'place_id' => $this->integer()->notNull(),
          'type_id' => $this->integer()->notNull(),
          'amount' => $this->integer(),
          'reason' => $this->text(),
          'FOREIGN KEY (user_id) REFERENCES {{%user}}(id)
              ON DELETE CASCADE ON UPDATE CASCADE',
          'FOREIGN KEY (place_id) REFERENCES {{%place}}(id)
              ON DELETE CASCADE ON UPDATE CASCADE',
          'FOREIGN KEY (type_id) REFERENCES {{%donation_type}}(id)
              ON DELETE CASCADE ON UPDATE CASCADE',
      ], $tableOptions);

      $this->createTable('{{%blood_declaration}}', [
          'id' => $this->primaryKey(),
          'user_id' => $this->integer()->notNull(),
          'place_id' => $this->integer()->notNull(),
          'type_id' => $this->integer()->notNull(),
          'FOREIGN KEY (user_id) REFERENCES {{%user}}(id)
              ON DELETE CASCADE ON UPDATE CASCADE',
          'FOREIGN KEY (place_id) REFERENCES {{%place}}(id)
              ON DELETE CASCADE ON UPDATE CASCADE',
          'FOREIGN KEY (type_id) REFERENCES {{%donation_type}}(id)
              ON DELETE CASCADE ON UPDATE CASCADE',
      ], $tableOptions);

      $this->createTable('{{%blood_amount_info}}', [
          'id' => $this->primaryKey(),
          'place_id' => $this->integer()->notNull(),
          'group_id' => $this->integer()->notNull(),
          'amount_type_id' => $this->integer()->notNull(),
          'parsedat' => $this->integer()->notNull(),
          'FOREIGN KEY (place_id) REFERENCES {{%place}}(id)
              ON DELETE CASCADE ON UPDATE CASCADE',
          'FOREIGN KEY (amount_type_id) REFERENCES {{%parsed_amount_type}}(id)
              ON DELETE CASCADE ON UPDATE CASCADE',
          'FOREIGN KEY (group_id) REFERENCES {{%blood_group}}(id)
              ON DELETE CASCADE ON UPDATE CASCADE',
      ], $tableOptions);

      $this->createTable('{{%blood_request}}', [
          'id' => $this->primaryKey(),
          'user_id' => $this->integer()->notNull(),
          'place_id' => $this->integer()->notNull(),
          'type_id' => $this->integer()->notNull(),
          'for_who' => $this->text(),
          'description' => $this->string()->notNull(),
          'FOREIGN KEY (user_id) REFERENCES {{%user}}(id)
              ON DELETE CASCADE ON UPDATE CASCADE',
          'FOREIGN KEY (place_id) REFERENCES {{%place}}(id)
              ON DELETE CASCADE ON UPDATE CASCADE',
          'FOREIGN KEY (type_id) REFERENCES {{%donation_type}}(id)
              ON DELETE CASCADE ON UPDATE CASCADE',
      ], $tableOptions);

      $this->createTable('{{%blood_request_answers}}', [
          'id' => $this->primaryKey(),
          'user_id' => $this->integer()->notNull(),
          'request_id' => $this->integer()->notNull(),
          'FOREIGN KEY (user_id) REFERENCES {{%user}}(id)
              ON DELETE CASCADE ON UPDATE CASCADE',
          'FOREIGN KEY (request_id) REFERENCES {{%blood_request}}(id)
              ON DELETE CASCADE ON UPDATE CASCADE',
      ], $tableOptions);
  }

  public function down()
  {
      $this->dropTable('{{%blood_request_answers}}');
      $this->dropTable('{{%blood_request}}');
      $this->dropTable('{{%blood_amount_info}}');
      $this->dropTable('{{%blood_declaration}}');
      $this->dropTable('{{%blood_history}}');
      $this->dropTable('{{%personal_info}}');
  }
  
}
