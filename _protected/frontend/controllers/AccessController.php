<?php
namespace frontend\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use Yii;

/**
 * FrontendController extends Controller and implements the behaviors() method
 * where you can specify the access control ( AC filter + RBAC) for
 * your controllers and their actions.
 */
class AccessController extends Controller
{
    public $layout = "panel";

    /**
     * Returns a list of behaviors that this component should behave as.
     * Here we use RBAC in combination with AccessControl filter.
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    //---------- ALL CAN SEE THAT  ----------//
                    [
                        'actions' => ['error'],
                        'allow' => true,
                    ],
                    //---------- DIRECT ACCESS CONTROL ----------//
                    [
                        'controllers' => ['site'],
                        'actions' => ['signup','login','index','request-password-reset'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                    [
                        'controllers' => ['site'],
                        'actions' => ['logout','panel', 'declarations'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    //---------- ADMIN ACCESS CONTROL ----------//
                    [
                        'controllers' => ['bloodrequest', 'studyresult','bloodrequestanswers','bloodhistory','blooddeclaration','gender'],
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'admin'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    //---------- USER ACCESS CONTROL ----------//
                    [
                        'controllers' => ['bloodrequest', 'studyresult','bloodrequestanswers','bloodhistory','blooddeclaration'],
                        'actions' => ['view', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['manageOwn'],
                    ],
                    /*
                    [
                        'controllers' => ['site'],
                        'actions' => ['profile'],
                        'allow' => true,
                        'roles' => ['manageOwn'],
                    ],
                    */
                    [
                        'controllers' => ['bloodrequest', 'studyresult','bloodrequestanswers','bloodhistory','blooddeclaration'],
                        'actions' => ['index','create','overall','requests','answers'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    /*
                    [
                        'controllers' => ['personalinfo'],
                        'actions' => ['view', 'create', 'update'],
                        'allow' => true,
                        'roles' => ['updateOwnPersonalInfo'],
                    ],
                    //---------- OTHERS ACCESS CONTROL ----------//
                    [
                        'controllers' => ['blooddeclaration'],
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['manageDeclarations'],
                    ],
                    [
                        'controllers' => ['bloodgroup'],
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['manageBloodGroups'],
                    ],
                    [
                        'controllers' => ['bloodhistory'],
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['manageBloodHistories'],
                    ],
                    [
                        'controllers' => ['bloodrequestanswers'],
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['manageBloodRequestsAnswers'],
                    ],
                    [
                        'controllers' => ['bloodrequest'],
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['manageBloodRequests'],
                    ],
                    [
                        'controllers' => ['donationtype'],
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['manageDonationTypes'],
                    ],
                    [
                        'controllers' => ['parsedamounttype'],
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['manageParsedAmountTypes'],
                    ],
                    [
                        'controllers' => ['personalinfo'],
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['managePersonalInfos'],
                    ],
                    [
                        'controllers' => ['place'],
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['managePlaces'],
                    ],
                    [
                        'controllers' => ['studyresult'],
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['manageStudyResults'],
                    ],
                    */
                    //---------- EVERYTHING ELSE IS DENIED!  ----------//
                ], // rules
            ], // access
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'logout' => ['post'],
                ],
            ], // verbs

        ]; // return

    } // behaviors

} // AppController
