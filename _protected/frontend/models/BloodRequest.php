<?php

namespace frontend\models;

use Yii;
use yii\web\Link;
use yii\web\Linkable;
use yii\helpers\Url;

/**
 * This is the model class for table "blood_request".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $place_id
 * @property integer $type_id
 * @property string $for_who
 * @property string $description
 *
 * @property User $user
 * @property Place $place
 * @property DonationType $type
 * @property BloodRequestAnswers[] $bloodRequestAnswers
 */
class BloodRequest extends \yii\db\ActiveRecord implements Linkable
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blood_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'place_id', 'type_id', 'description'], 'required'],
            [['user_id', 'place_id', 'type_id'], 'integer'],
            [['for_who'], 'string'],
            [['description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'place_id' => Yii::t('app', 'Place ID'),
            'type_id' => Yii::t('app', 'Type ID'),
            'for_who' => Yii::t('app', 'For Who'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     *  Filter fields for the REST API
     *  @return $fields
     */
    public function fields()
    {
        $fields = parent::fields();

        unset($fields['id']);

        return $fields;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlace()
    {
        return $this->hasOne(Place::className(), ['id' => 'place_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(DonationType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBloodRequestAnswers()
    {
        return $this->hasMany(BloodRequestAnswers::className(), ['request_id' => 'id']);
    }

    /**
     * @return REST link
     */
    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['bloodrequest/view', 'id' => $this->id], true),
            'user' => Url::to(['user/view', 'id' => $this->user_id], true),
            'place' => Url::to(['user/view', 'id' => $this->place_id], true),
            'donation_type' => Url::to(['user/view', 'id' => $this->type_id], true),
        ];
    }

}
